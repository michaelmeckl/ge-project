using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using UnityEngine.SceneManagement;


/// Code is based on the "Quick Object Replacer Tool" from the Unity AssetStore
/// (https://assetstore.unity.com/packages/tools/utilities/quick-object-replacer-tool-198397) and this
/// "SmartReplace" - Tool (https://github.com/Moolt/SmartReplace).
public class ReplacePrefabUtil : EditorWindow
{
    GameObject[] selectedObjects;
    GameObject replacementObject;
    
    bool copyPosition = true;
    bool copyRotation = true;
    bool copyScale = true;

    // whether to keep the name of the old prefab or use the name of the new one
    public bool keepNameOfOldPrefab = false;
    public bool enableMultiScene = false;

    private Vector2 scrollPosition;

    private string searchTag = "";
    private string searchName = "";
    private bool showSelectionResults = false;
    
    private GameObject[] selectedTagObjects = {};
    private GameObject[] selectedNameObjects = {};
    
    
    // enum for the options dropdown menu
    public enum SelectionOption
    {
        SelectInScene, SelectByTag, SelectByName
    }
    public SelectionOption selectionOption;
    
    
    // options how to deal with nested prefabs (as deleting nested prefabs might bring some irreversible changes)
    public enum HandlingNestedPrefabs
    {
        Destroy, SetActiveFalse, Ignore
    }
    public HandlingNestedPrefabs nestedPrefabOption;
    

    [MenuItem("Tools/Prefab Replacer")]
    public static void ShowWindow()
    {
        var editor = (ReplacePrefabUtil)GetWindow(typeof(ReplacePrefabUtil));
        editor.titleContent = new GUIContent("Prefab Replacer");
        editor.Show();
    }

    private void OnGUI()
    {
        EditorGUILayout.Space(3);
        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
        
        EditorGUILayout.LabelField("Choose option", EditorStyles.boldLabel);
        selectionOption = (SelectionOption) EditorGUILayout.EnumPopup(selectionOption);
        EditorGUILayout.Space();

        switch (selectionOption)
        {
            case SelectionOption.SelectByTag:
                GUILayout.Label("Usage:\nGive all game objects that should be replaced a certain tag.");
                
                EditorGUILayout.Space(15);
                searchTag = EditorGUILayout.TextField("Tag: ", searchTag);
                EditorGUILayout.Space();
                
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("", GUILayout.Width(15));
                if (GUILayout.Button("Select objects", GUILayout.Width(200)))
                {
                    var allObjects = FindAllObjects();
                    FindSimilarObjectsByTag(allObjects);  // modifies 'allObjects'

                    selectedTagObjects = allObjects.ToArray();
                    if (allObjects.Count > 0)
                    {
                        Selection.objects = selectedTagObjects as UnityEngine.Object[];  // highlight found objects in scene
                    }
                }
                EditorGUILayout.EndHorizontal();
                
                EditorGUILayout.Space(15);
                showSelectionResults = EditorGUILayout.Foldout(showSelectionResults, "Found objects for this tag:");
                
                if (showSelectionResults)
                {
                    foreach (var foundObject in selectedTagObjects)
                    {
                        if (!foundObject) 
                            continue;
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("", GUILayout.Width(25));
                        GUILayout.Label($"*  {foundObject.name}");
                        EditorGUILayout.EndHorizontal();
                    }
                }
                EditorGUILayout.Space(15);
                
                break;
            case SelectionOption.SelectByName:
                GUILayout.Label("Usage:\nEnter a name that all objects that should be replaced have in common.");
                
                EditorGUILayout.Space(15);
                searchName = EditorGUILayout.TextField("Name: ", searchName);
                EditorGUILayout.Space();
                
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("", GUILayout.Width(15));
                if (GUILayout.Button("Select objects", GUILayout.Width(200)))
                {
                    var allObjects = FindAllObjects();
                    FindSimilarObjectsByName(allObjects);  // modifies 'allObjects'

                    selectedNameObjects = allObjects.ToArray();
                    if (allObjects.Count > 0)
                    {
                        Selection.objects = selectedNameObjects as UnityEngine.Object[];  // highlight found objects in scene
                    }
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space(15);
                showSelectionResults = EditorGUILayout.Foldout(showSelectionResults, "Found objects for this name:");
                
                if (showSelectionResults)
                {
                    foreach (var foundObject in selectedNameObjects)
                    {
                        if (!foundObject) 
                            continue;
                        EditorGUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("", GUILayout.Width(25));
                        GUILayout.Label($"-  {foundObject.name}");
                        EditorGUILayout.EndHorizontal();
                    }
                }
                EditorGUILayout.Space(15);
                
                break;
            case SelectionOption.SelectInScene:
                GUILayout.Label("Usage:\nSelect all game objects in the scene that should be replaced.");
                break;
        }
        
        EditorGUILayout.Space(15);
        // draw a horizontal line
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        
        replacementObject = (GameObject) EditorGUILayout.ObjectField("Replace with: ", replacementObject, 
            typeof(GameObject), true, GUILayout.MaxWidth(300));
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Transform Settings", EditorStyles.boldLabel);
        copyPosition = EditorGUILayout.Toggle("Keep Position", copyPosition);
        copyRotation = EditorGUILayout.Toggle("Keep Rotation", copyRotation);
        copyScale = EditorGUILayout.Toggle("Keep Scale", copyScale);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Other", EditorStyles.boldLabel);

        enableMultiScene = EditorGUILayout.Toggle("Allow multi scene replace", enableMultiScene);
        keepNameOfOldPrefab = EditorGUILayout.Toggle("Keep name of old object", keepNameOfOldPrefab);
        
        nestedPrefabOption = (HandlingNestedPrefabs) EditorGUILayout.EnumPopup("Handling nested prefabs: ", nestedPrefabOption);

        EditorGUILayout.Space(15);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("", GUILayout.Width(60));  // use empty label field as a way for indentation
        if (GUILayout.Button("Replace", GUILayout.Width(200)))
        {
            ReplaceAll();
        }
        EditorGUILayout.Space(35);
        
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndScrollView();
    }
    

    // Called by the replace button
    private void ReplaceAll()
    {
        switch (selectionOption)
        {
            case SelectionOption.SelectInScene:
                selectedObjects = Selection.gameObjects;
                break;
            case SelectionOption.SelectByTag:
                selectedObjects = selectedTagObjects;
                break;
            case SelectionOption.SelectByName:
                selectedObjects = selectedNameObjects;
                break;
        }
        
        // taken from https://assetstore.unity.com/packages/tools/utilities/quick-object-replacer-tool-198397
        string prefabType = PrefabUtility.GetPrefabAssetType(replacementObject).ToString();
        string instanceStatus = null;
        if (prefabType is "Regular" or "Variant")
            instanceStatus = PrefabUtility.GetPrefabInstanceStatus(replacementObject).ToString();
        
        // Debug.Log("Prefab type: " + prefabType + "; Instance status: "+ instanceStatus);
        
        foreach (var selectedObject in selectedObjects)
        {
            Replace(selectedObject, prefabType, instanceStatus);
        }
    }

    // Replaces a single instance of a given object
    private void Replace(GameObject oldObject, in string prefabType, in string instanceStatus)
    {
        // Instantiate new prefab
        GameObject newlyCreatedPrefab = null;
        
        if (prefabType is "Regular" or "Variant")
        {
            if (instanceStatus == "Connected")
            {
                // taken from https://assetstore.unity.com/packages/tools/utilities/quick-object-replacer-tool-198397
                var newPrefab = PrefabUtility.GetCorrespondingObjectFromSource(replacementObject);
                newlyCreatedPrefab = PrefabUtility.InstantiatePrefab(newPrefab, oldObject.scene) as GameObject;
                PrefabUtility.SetPropertyModifications(newlyCreatedPrefab, PrefabUtility.GetPropertyModifications(replacementObject));
            }
            else
            {
                newlyCreatedPrefab = PrefabUtility.InstantiatePrefab(replacementObject, oldObject.scene) as GameObject;
            }
        }
        else
        {
            newlyCreatedPrefab = Instantiate(replacementObject);
        }

        if (!newlyCreatedPrefab)
        {
            Debug.LogError("Error in creating new prefab! New prefab is null!");
            return;
        }

        // Apply old transformations if checked
        if (copyPosition)
            newlyCreatedPrefab.transform.position = oldObject.transform.position;
        if (copyRotation)
            newlyCreatedPrefab.transform.rotation = oldObject.transform.rotation;
        if (copyScale)
            newlyCreatedPrefab.transform.localScale = oldObject.transform.localScale;

        // Set the name of the old prefab on the new one if checked
        if (keepNameOfOldPrefab)
        {
            newlyCreatedPrefab.name = oldObject.name;
        }
        
        // use Undo methods to allow Ctrl + Z to undo the change
        Undo.RegisterCreatedObjectUndo(newlyCreatedPrefab, "undo replace objects");

        // Needed to invalidate the scene
        Undo.RecordObject(oldObject, "Replace object");

        // use try catch in case of nested prefabs, see https://forum.unity.com/threads/a-script-replacing-one-prefab-with-another.681805/#post-4878257
        try
        {
            // get the parent of the object that should be replaced if it has one
            var oldObjectParent = oldObject.transform.parent;
            if (oldObjectParent != null)
            {
                // and set the newly created prefab as child of this parent at the same position
                Undo.SetTransformParent(newlyCreatedPrefab.transform, oldObjectParent.transform, true, "Set new parent");
            }
            
            Undo.DestroyObjectImmediate(oldObject);
        }
        catch
        {
            if (PrefabUtility.IsPartOfAnyPrefab(oldObject))
            {
                // Debug.Log("Trying to delete child prefab of another prefab ...");
                
                var parentPrefab = PrefabUtility.GetOutermostPrefabInstanceRoot(oldObject);
                var assetPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(parentPrefab);

                if (!string.IsNullOrEmpty(assetPath))
                {
                    // use editingScope to apply all modifications at the end automatically to the prefab
                    using var editingScope = new PrefabUtility.EditPrefabContentsScope(assetPath);
                    var prefabRoot = editingScope.prefabContentsRoot;

                    // Modify Prefab contents.
                    var childToDelete = prefabRoot.transform.Find(oldObject.name);

                    switch (nestedPrefabOption)
                    {
                        case HandlingNestedPrefabs.Destroy:
                            try
                            {
                                Undo.DestroyObjectImmediate(childToDelete.gameObject);
                            }
                            catch
                            {
                                Debug.LogError("Couldn't delete child prefab!");
                            }
                            break;
                        case HandlingNestedPrefabs.SetActiveFalse:
                            childToDelete.gameObject.SetActive(false);
                            break;
                        case HandlingNestedPrefabs.Ignore:
                            break;
                    }
                    
                    // IMPORTANT: set 'worldPositionStays' parameter to false to keep the original position in relation to the parent
                    Undo.SetTransformParent(newlyCreatedPrefab.transform, prefabRoot.transform, false, "Set new prefab parent");
                }
            }
            else
            {
                Debug.LogError($"Couldn't replace old object \"{oldObject.name}\"!");
            }
        }
    }

    #region ReplaceByTag

    // adapted from https://gamedev.stackexchange.com/questions/133797/replacing-all-objects-in-scene-with-another-object
    private void FindSimilarObjectsByTag(List<GameObject> objectsToBeFiltered)
    {
        if (string.IsNullOrEmpty(searchTag))
        {
            Debug.LogWarning("Can't find results because tag name is empty!");
            objectsToBeFiltered.Clear();
            return;
        }

        // check all available tags first, see https://answers.unity.com/questions/57952/getting-the-tag-array.html
        // TODO undocumented feature, might break in future Unity versions
        if (!UnityEditorInternal.InternalEditorUtility.tags.Contains(searchTag))
        {
            Debug.LogWarning($"No available tag with the name \"{searchTag}\" was found!");
            objectsToBeFiltered.Clear();
            return;
        }

        var objectsWithTag = GameObject.FindGameObjectsWithTag(searchTag);
        // objectsToBeFiltered.RemoveAll(o => !objectsWithTag.Contains(o));
        // objectsToBeFiltered = objectsWithTag.ToList();
        objectsToBeFiltered.Clear();
        objectsToBeFiltered.AddRange(objectsWithTag);
    }
    
    #endregion
    
    
    #region ReplaceByName
    
    private void FindSimilarObjectsByName(List<GameObject> objectsToBeFiltered)
    {
        if (string.IsNullOrEmpty(searchName))
        {
            Debug.LogWarning("Can't find results because name is empty!");
            objectsToBeFiltered.Clear();
            return;
        }

        var searchNameLower = searchName.ToLower();
        var nameList = objectsToBeFiltered.Select(x=>x.name);
        
        // get all objects in the name list that don't contain the searchName
        var objectsToRemove = nameList.Where(objName => !objName.ToLower().Contains(searchNameLower)).ToList();
        
        // and remove those from all objects
        objectsToBeFiltered.RemoveAll(o => objectsToRemove.Contains(o.name));
    }

    #endregion

    
    // Will return a list of all objects present in the scene view
    // Will remove objects that are not in the same scene as brokenPrefab if enableMultiScene is disabled
    // Taken from https://github.com/Moolt/SmartReplace
    private List<GameObject> FindAllObjects()
    {
        var allObjects = FindObjectsOfType<GameObject>().ToList();
        
        var currentScene = SceneManager.GetActiveScene();
        if (!enableMultiScene)
        {
            allObjects.RemoveAll(o => o.scene.name != currentScene.name);
        }

        return allObjects;
    }
}
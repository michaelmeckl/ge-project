using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

namespace StarterAssets
{
	public class StarterAssetsInputs : MonoBehaviour
	{
		[Header("Character Input Values")]
		public Vector2 move;
		public Vector2 look;
		public bool jump;
		public bool attack;
		public bool sneak;
		public bool sprint;

		public bool firstpersoncamera;
		public bool thirdpersoncamera;
		public bool upperthirdpersoncamera;

		public bool leanLeft;
		public bool leanRight;

		public bool pause;
		public bool interaction;

		[Header("Movement Settings")]
		public bool analogMovement;

#if !UNITY_IOS || !UNITY_ANDROID
		[Header("Mouse Cursor Settings")]
		public bool cursorLocked = true;
		public bool cursorInputForLook = true;
#endif

#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
		public void OnMove(InputValue value)
		{
			MoveInput(value.Get<Vector2>());
		}

		public void OnLook(InputValue value)
		{
			if(cursorInputForLook)
			{
				LookInput(value.Get<Vector2>());
			}
		}

		public void OnJump(InputValue value)
		{
			JumpInput(value.isPressed);
		}

		public void OnAttack(InputValue value)
		{
			AttackInput(value.isPressed);
		}

		public void OnSprint(InputValue value)
		{
			SprintInput(value.isPressed);
		}

		public void OnSneak(InputValue value)
		{
			SneakInput(value.isPressed);
		}

		public void OnCameraChangeFirstPerson(InputValue value)
        {
			CameraChangeFirstPersonInput(value.isPressed);
        }

		public void OnCameraChangeThirdPerson(InputValue value)
		{
			CameraChangeThirdPersonInput(value.isPressed);
		}

		public void OnCameraChangeUpperThirdPerson(InputValue value)
		{
			CameraChangeUpperThirdPersonInput(value.isPressed);
		}

		public void OnLeanLeft(InputValue value)
		{
			LeanLeft(value.isPressed);
		}

		public void OnLeanRight(InputValue value)
		{
			LeanRight(value.isPressed);
		}

		public void OnPause(InputValue value)
        {
			Pause(value.isPressed);
		}

		public void OnInteraction(InputValue value)
		{
			Interaction(value.isPressed);
		}
#else
	// old input sys if we do decide to have it (most likely wont)...
#endif


		public void MoveInput(Vector2 newMoveDirection)
		{
			move = newMoveDirection;
		} 

		public void LookInput(Vector2 newLookDirection)
		{
			look = newLookDirection;
		}

		public void JumpInput(bool newJumpState)
		{
			jump = newJumpState;
		}

		public void AttackInput(bool newAttackState)
		{
			attack = newAttackState;
		}

		public void SprintInput(bool newSprintState)
		{
			sprint = newSprintState;
		}

		public void SneakInput(bool newSneakState)
		{
			sneak = newSneakState;
		}

		public void CameraChangeFirstPersonInput(bool newCameraChangeFirstPersonState)
		{
			firstpersoncamera = newCameraChangeFirstPersonState;
		}

		public void CameraChangeThirdPersonInput(bool newCameraChangeThirdPersonState)
		{
			thirdpersoncamera = newCameraChangeThirdPersonState;
		}

		public void CameraChangeUpperThirdPersonInput(bool newCameraChangeUpperThirdPersonState)
		{
			upperthirdpersoncamera = newCameraChangeUpperThirdPersonState;
		}

		public void LeanLeft(bool newLeanLeftState)
		{
			leanLeft = newLeanLeftState;
		}

		public void LeanRight(bool newLeanRightState)
		{
			leanRight = newLeanRightState;
		}

		public void Pause(bool newPauseState)
		{
			pause = newPauseState;
		}

		public void Interaction(bool newInteractionState)
		{
			interaction = newInteractionState;
		}

#if !UNITY_IOS || !UNITY_ANDROID

		private void OnApplicationFocus(bool hasFocus)
		{
			SetCursorState(cursorLocked);
		}

		private void SetCursorState(bool newState)
		{
			Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
		}

#endif

	}
	
}
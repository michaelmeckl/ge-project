All Sketchfab Models listed below are used in a voxelized form (Voxelized with https://assetstore.unity.com/packages/tools/utilities/magicavoxel-tools-free-146116)
See the Models and Prefabs folders for the actual models.
------------------------------------------------------------------------------------------


- "Combination Lock" (https://skfb.ly/6SrwZ) by ratqi is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).

- "Detail Scroll" (https://skfb.ly/69Tzw) by AntonioD is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).

- "Scroll 0.35x0.1x0.1" (https://skfb.ly/69VAs) by moyicat is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).

- "Collector's Display Case (GOTG Mission Breakout)" (https://sketchfab.com/3d-models/collectors-display-case-gotg-mission-breakout-7144957c6f8f46bb8c0ea14a4ebdda75) by Kevin Mar (https://sketchfab.com/Taiyaki1199) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)

- "Steampunk book" (https://skfb.ly/6z6BL) by TonyGeneralist is licensed under Creative Commons Attribution-NonCommercial (http://creativecommons.org/licenses/by-nc/4.0/).

- "steampunk cube" (https://sketchfab.com/3d-models/steampunk-cube-bf14dfeb101348c6b0d13f94dba732ca) by AndreuSeara (https://sketchfab.com/AndreuSeara) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)

- "Strange Machine" (https://sketchfab.com/3d-models/strange-machine-2031b46ff0864f3ea17fe60461f6b58b) by Batuhan13 (https://sketchfab.com/Batuhan13) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)

- "Riot Police Mech" (https://skfb.ly/6yxUo) by UselessViking is licensed under Creative Commons Attribution-NonCommercial (http://creativecommons.org/licenses/by-nc/4.0/).

- "Steampunk Lamp" (https://skfb.ly/6QV9r) by nikita.bulgakov is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).


---------------------------- Not Used at the moment -----------------------------------

- "Simple Sci-Fi Wall Panel" (https://skfb.ly/6VoHM) by Mizuchi Sensei is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).

- "Steampunk Spider Robot" (https://sketchfab.com/3d-models/steampunk-spider-robot-9ceb205bf7c549abb9a296cd5143360c) by yanix (https://sketchfab.com/yanix) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)


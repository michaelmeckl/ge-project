
public class HealthSystem
{
    private int health;
    private int healthMax;

    public HealthSystem(int health)
    {
        this.healthMax = health;
        this.health = healthMax;
    }

    public int GetHealth()
    {
        return health;
    }

    public float GetHealthPercent()
    {
        return (float)health / healthMax;
    }

    public void Damage(int damage)
    {
        health -= damage;
        if (health < 0) health = 0;
    }

    public void Heal(int heal)
    {
        health += heal;
        if (health >= healthMax) health = healthMax;
    }
}

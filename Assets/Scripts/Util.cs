using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util
{
    public static void Shuffle<T>(List<T> inputList)
    {
        for (int i = 0; i < inputList.Count; i++)
        {
            T temp = inputList[i];
            int rand = Random.Range(i, inputList.Count);
            inputList[i] = inputList[rand];
            inputList[rand] = temp;
        }
    }

    public static void Shuffle<T>(T[] inputList)
    {
        for (int i = 0; i < inputList.Length; i++)
        {
            T temp = inputList[i];
            int rand = Random.Range(i, inputList.Length);
            inputList[i] = inputList[rand];
            inputList[rand] = temp;
        }
    }
}

using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    public ThirdPersonController CharacterController;
    public CameraController CameraController;
    public GameHandler CharacterHandler;
    public HidePopup PopUp;
}

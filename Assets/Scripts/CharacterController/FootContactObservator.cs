using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootContactObservator : MonoBehaviour
{
    public delegate void FootDown(AnimationEvent anim);

    public FootDown OnFootDown;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AnimFootDown(AnimationEvent anim)
    {
        OnFootDown.Invoke(anim);
    }

}

public enum Foot
{
    Left,
    Right
}

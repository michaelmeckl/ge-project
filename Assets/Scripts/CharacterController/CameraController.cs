using Cinemachine;
using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class CameraController : MonoBehaviour
{
    public GameController Controller;
    public StarterAssetsInputs Input;
    public ThirdPersonController ThirdPersonController;
    public Camera MainCamera;
    public CinemachineStateDrivenCamera StateDrivenCamera;
    public Volume PostProcessing;
    public GameObject Sword;

    public delegate void OnCameramoveFinish();
    public OnCameramoveFinish Callback;

    public bool LockedPerspectiveChange = false;

    private ColorAdjustments _colorPost;
    private MotionBlur _motionBlur;
    private LensDistortion _lensDistortion;
    private FilmGrain _filmGrain;

    private CinemachineVirtualCamera _firstPersonCamera;
    private CinemachineVirtualCamera _thirdPersonCamera;
    private CinemachineVirtualCamera _upperPersonCamera;
    private CinemachineVirtualCamera _leanLeftFirstCamera;
    private CinemachineVirtualCamera _leanRightFirstCamera;
    private CinemachineVirtualCamera _leanLeftThirdCamera;
    private CinemachineVirtualCamera _leanRightThirdCamera;
    private CinemachineVirtualCamera _startCamera;

    private Animator _animator;

    private bool _isBlending;
    private bool _started;
    private Transform _swordTransform;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _isBlending = false;
        _started = false;
        _firstPersonCamera = (CinemachineVirtualCamera)StateDrivenCamera.ChildCameras.FirstOrDefault(x => x.Name == "FirstPersonCamera");
        _thirdPersonCamera = (CinemachineVirtualCamera)StateDrivenCamera.ChildCameras.FirstOrDefault(x => x.Name == "ThirdPersonCamera");
        _upperPersonCamera = (CinemachineVirtualCamera)StateDrivenCamera.ChildCameras.FirstOrDefault(x => x.Name == "UpperCamera");
        _leanLeftFirstCamera = (CinemachineVirtualCamera)StateDrivenCamera.ChildCameras.FirstOrDefault(x => x.Name == "LeanLeftFirstCamera");
        _leanRightFirstCamera = (CinemachineVirtualCamera)StateDrivenCamera.ChildCameras.FirstOrDefault(x => x.Name == "LeanRightFirstCamera");
        _leanLeftThirdCamera = (CinemachineVirtualCamera)StateDrivenCamera.ChildCameras.FirstOrDefault(x => x.Name == "LeanLeftThirdCamera");
        _leanRightThirdCamera = (CinemachineVirtualCamera)StateDrivenCamera.ChildCameras.FirstOrDefault(x => x.Name == "LeanRightTHirdCamera");
        _startCamera = (CinemachineVirtualCamera)StateDrivenCamera.ChildCameras.FirstOrDefault(x => x.Name == "StartCamera");

        PostProcessing.profile.TryGet(out _colorPost);
        PostProcessing.profile.TryGet(out _motionBlur);
        PostProcessing.profile.TryGet(out _lensDistortion);
        PostProcessing.profile.TryGet(out _filmGrain);
        _colorPost.active = false;
        _lensDistortion.active = false;
        _filmGrain.active = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(_started)
        {
            UpdateCamera();
        }
    }

    //This code needs some serious purification...
    private void UpdateCamera()
    {
        if(StateDrivenCamera.IsBlending)
        {
            if(!_isBlending)
            {
                _isBlending = true;
            }
        }
        else
        {
            if (_isBlending)
            {
                _isBlending = false;
                OnCameraMoveFinish();
            }
        }

        if(ThirdPersonController.Sneak)
        {
            if (ThirdPersonController.Perspective == CameraPerspective.First)
            {
                _firstPersonCamera.Follow = ThirdPersonController.CinemachineCameraTargetFirstSneak.transform;
            }          
        }
        else
        {
            if (ThirdPersonController.Perspective == CameraPerspective.First)
            {
                _firstPersonCamera.Follow = ThirdPersonController.CinemachineCameraTargetFirst.transform;
            }
        }

        if(Input.leanLeft)
        {
            if(ThirdPersonController.Perspective == CameraPerspective.First)
            {
                ThirdPersonController.Leaning = true;
                LockedPerspectiveChange = true;
                _animator.Play("LeanLeftFirst");
                Input.cursorInputForLook = false;
            }
            else if(ThirdPersonController.Perspective == CameraPerspective.Third)
            {
                ThirdPersonController.Leaning = true;
                LockedPerspectiveChange = true;
                _animator.Play("LeanLeftThird");
                Input.cursorInputForLook = false;
            }
        }
        else if(Input.leanRight)
        {
            if (ThirdPersonController.Perspective == CameraPerspective.First)
            {
                ThirdPersonController.Leaning = true;
                LockedPerspectiveChange = true;
                _animator.Play("LeanRightFirst");
                Input.cursorInputForLook = false;
            }
            else if (ThirdPersonController.Perspective == CameraPerspective.Third)
            {
                ThirdPersonController.Leaning = true;
                LockedPerspectiveChange = true;
                _animator.Play("LeanRightThird");
                Input.cursorInputForLook = false;
            }
        }
        else
        {
            if (ThirdPersonController.Perspective == CameraPerspective.First)
            {
                ThirdPersonController.Leaning = false;
                LockedPerspectiveChange = false;
                _animator.Play("FirstPerson");
                Input.cursorInputForLook = true;
            }
            else if (ThirdPersonController.Perspective == CameraPerspective.Third)
            {
                ThirdPersonController.Leaning = false;
                LockedPerspectiveChange = false;
                _animator.Play("ThirdPerson");
                Input.cursorInputForLook = true;
            }
        }

        if (Controller.LockedInput || LockedPerspectiveChange || ThirdPersonController.LockPerspective)
        {
            Input.firstpersoncamera = false;
            Input.thirdpersoncamera = false;
            Input.upperthirdpersoncamera = false;
            return;
        }

        if (Input.firstpersoncamera)
        {
            switch(ThirdPersonController.Perspective)
            {
                case CameraPerspective.First:
                    break;
                case CameraPerspective.Third:
                    Controller.LockInput();
                    _animator.Play("FirstPerson");
                    _isBlending = true;
                    ThirdPersonController.Perspective = CameraPerspective.First;

                    _swordTransform = Sword.transform;
                    Sword.transform.localPosition = new Vector3(-1.288f, -2.39f, -0.877f);
                    CameraPostProcessing(true);
                    Controller.PauseGameSpeed();
                    break;
                case CameraPerspective.UpperThird:
                    Controller.LockInput();
                    _animator.Play("FirstPerson");
                    ThirdPersonController.Perspective = CameraPerspective.First;
                    _isBlending = true;

                    _swordTransform = Sword.transform;
                    Sword.transform.localPosition = new Vector3(-1.288f, -2.39f, -0.877f);

                    CameraPostProcessing(true);
                    Controller.PauseGameSpeed();
                    break;
            }
        }
        else if (Input.thirdpersoncamera)
        {
            MainCamera.cullingMask =
                (1 << LayerMask.NameToLayer("TransparentFX")) |
                (1 << LayerMask.NameToLayer("Default")) |
                (1 << LayerMask.NameToLayer("IgnoreRaycast")) |
                (1 << LayerMask.NameToLayer("Character")) |
                (1 << LayerMask.NameToLayer("Water")) |
                (1 << LayerMask.NameToLayer("UI")) |
                (1 << LayerMask.NameToLayer("Obstacle")) |
                (1 << LayerMask.NameToLayer("PostProcessing"));
            switch (ThirdPersonController.Perspective)
            {
                case CameraPerspective.First:
                    Controller.LockInput();
                    _animator.Play("ThirdPerson");
                    ThirdPersonController.Perspective = CameraPerspective.Third;
                    _isBlending = true;

                    Sword.transform.localPosition = Vector3.zero;
                    CameraPostProcessing(true);
                    Controller.PauseGameSpeed();
                    break;
                case CameraPerspective.Third:
                    break;
                case CameraPerspective.UpperThird:
                    Controller.LockInput();
                    _animator.Play("ThirdPerson");
                    ThirdPersonController.Perspective = CameraPerspective.Third;
                    _isBlending = true;
                    CameraPostProcessing(true);
                    Controller.PauseGameSpeed();
                    break;
            }
        }
        else if(Input.upperthirdpersoncamera)
        {
            MainCamera.cullingMask =
                (1 << LayerMask.NameToLayer("TransparentFX")) |
                (1 << LayerMask.NameToLayer("Default")) |
                (1 << LayerMask.NameToLayer("IgnoreRaycast")) |
                (1 << LayerMask.NameToLayer("Character")) |
                (1 << LayerMask.NameToLayer("Water")) |
                (1 << LayerMask.NameToLayer("UI")) |
                (1 << LayerMask.NameToLayer("Obstacle")) |
                (1 << LayerMask.NameToLayer("PostProcessing"));
            switch (ThirdPersonController.Perspective)
            {
                case CameraPerspective.First:
                    Controller.LockInput();
                    _animator.Play("UpperPerson");
                    ThirdPersonController.Perspective = CameraPerspective.UpperThird;
                    _isBlending = true;

                    Sword.transform.localPosition = Vector3.zero;
                    CameraPostProcessing(true);
                    Controller.PauseGameSpeed();
                    break;
                case CameraPerspective.Third:
                    Controller.LockInput();
                    _animator.Play("UpperPerson");
                    ThirdPersonController.Perspective = CameraPerspective.UpperThird;
                    _isBlending = true;
                    CameraPostProcessing(true);
                    Controller.PauseGameSpeed();
                    break;
                case CameraPerspective.UpperThird:
                    break;
            }
        }
        Input.firstpersoncamera = false;
        Input.thirdpersoncamera = false;
        Input.upperthirdpersoncamera = false;
    }

    private void CameraPostProcessing(bool on)
    {
        if(on)
        {
            _colorPost.active = true;
            _motionBlur.intensity.value = 1.0f;
            _lensDistortion.active = true;
            _lensDistortion.intensity.Interp(0.0f, 0.2f, 0.2f);
            _filmGrain.active = true;
        }
        else
        {
            _colorPost.active = false;
            _motionBlur.intensity.value = 0.2f;
            _lensDistortion.active = true;
            _lensDistortion.intensity.Interp(0.2f, 0.0f, 0.2f);
            _filmGrain.active = false;
        }
    }

    public void OnCameraMoveFinish()
    {
        switch (ThirdPersonController.Perspective)
        {
            case CameraPerspective.First:
                MainCamera.cullingMask =
                    (1 << LayerMask.NameToLayer("TransparentFX")) |
                    (1 << LayerMask.NameToLayer("Default")) |
                    (1 << LayerMask.NameToLayer("IgnoreRaycast")) |
                    (1 << LayerMask.NameToLayer("Water")) |
                    (1 << LayerMask.NameToLayer("UI")) |
                    (1 << LayerMask.NameToLayer("Obstacle")) |
                    (1 << LayerMask.NameToLayer("PostProcessing"));
                break;
            case CameraPerspective.Third:
                MainCamera.cullingMask =
                    (1 << LayerMask.NameToLayer("TransparentFX")) |
                    (1 << LayerMask.NameToLayer("Default")) |
                    (1 << LayerMask.NameToLayer("IgnoreRaycast")) |
                    (1 << LayerMask.NameToLayer("Character")) |
                    (1 << LayerMask.NameToLayer("Water")) |
                    (1 << LayerMask.NameToLayer("UI")) |
                    (1 << LayerMask.NameToLayer("Obstacle")) |
                    (1 << LayerMask.NameToLayer("PostProcessing"));
                break;
            case CameraPerspective.UpperThird:
                MainCamera.cullingMask =
                    (1 << LayerMask.NameToLayer("TransparentFX")) |
                    (1 << LayerMask.NameToLayer("Default")) |
                    (1 << LayerMask.NameToLayer("IgnoreRaycast")) |
                    (1 << LayerMask.NameToLayer("Character")) |
                    (1 << LayerMask.NameToLayer("Water")) |
                    (1 << LayerMask.NameToLayer("UI")) |
                    (1 << LayerMask.NameToLayer("Obstacle")) |
                    (1 << LayerMask.NameToLayer("PostProcessing"));
                break;
        }
        CameraPostProcessing(false);
        Controller.FreeInput();
        Controller.ResumeGameSpeed();
        Callback?.Invoke();
    }

    public void StartToPov()
    {
        switch (ThirdPersonController.Perspective)
        {
            case CameraPerspective.First:
                _animator.Play("FirstPerson");
                CameraPostProcessing(true);
                break;
            case CameraPerspective.Third:
                _animator.Play("ThirdPerson");
                CameraPostProcessing(true);
                break;
            case CameraPerspective.UpperThird:
                _animator.Play("UpperPerson");
                CameraPostProcessing(true);
                break;
        }
        Controller.LockInput();
        _started = true;
    }
}

public enum CameraPerspective
{
    First,
    Third,
    UpperThird
}

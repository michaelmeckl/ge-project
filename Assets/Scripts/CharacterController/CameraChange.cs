using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange : MonoBehaviour
{
    public KeyCode Change;
    private Animator Anim;

     void Start()
    {
        Anim = GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(Change))
        {
            Anim.SetTrigger("CameraChange");
        }
    }
}

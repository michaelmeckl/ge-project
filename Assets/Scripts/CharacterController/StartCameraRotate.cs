using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCameraRotate : MonoBehaviour
{
    public float Velocity = 0.1f;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up * Velocity);
    }
}

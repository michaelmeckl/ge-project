using StarterAssets;
using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.InputSystem;

public class LevelManager : MonoBehaviour
{
    internal GameController GameManager;

    internal bool FinishedLevel = false;

    [SerializeField]
    internal GameObject ElyPlayerPrefab;
    internal GameObject ElyPlayer;

    internal CinemachineVirtualCamera EndCamera;

    internal TaskType CurrentTask;

    private ReachEnd EndTrigger;
    private PortalTriggerObservator PortalTrigger;
    private bool _startLogging;

    internal float LoggingInterval = 2f;

    internal bool Paused = false;
    private int _countPaused;


    private float _current = 0;

    internal bool FirstPopup = false;

    internal bool Pausable = true;

    [SerializeField]
    private StarterAssetsInputs _input;
    [SerializeField]
    private BuildLevel _levelBuilder;
    [SerializeField]
    private Volume _characterPostProcessing;
    [SerializeField]
    private QuestionaireController _questionaireController;
    [SerializeField]
    private GameObject _inspectionUI;
    [SerializeField]
    private PauseController _pauseMenue;

    private TutorialController _tutorial;

    private bool _puzzleSolved = false;
    private bool _combatSolved = false;
    private bool _dexSolved = false;
    private bool _stealthSolved = false;

    // Start is called before the first frame update
    void Start()
    {
        _startLogging = false;
        _questionaireController.Callback += HideQuestionaire;

        _questionaireController.ParticipantID = GameManager.ParticipantID;
        _questionaireController.CondCamera = GameManager.GetCurrentCameraCondition();
        _questionaireController.LNumber = (int)GameManager.CurrentGameState;

        _pauseMenue = Instantiate(_pauseMenue);
        _pauseMenue.gameObject.SetActive(false);
        _pauseMenue.StartButtonCallback += ResumeGame;
        CurrentTask = TaskType.None;
        _countPaused = 0;
        Pausable = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager is null || ElyPlayer is null)
        {
            return;
        }
        if(_startLogging)
        {
            var delta = Time.deltaTime;
            _current += delta;
            if(_current >= LoggingInterval)
            {
                _current -= LoggingInterval;
                LoggingSystem.AppendDataSet(new LogDataSet
                {
                    TimeStamp = DateTime.Now,
                    TimeStampUnixTime = ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds(),
                    ID = GameManager.ParticipantID,
                    ConditionCamera = GameManager.GetCurrentCameraCondition(),
                    LevelNumber = (int) GameManager.CurrentGameState,
                    UsedPerspective = ElyPlayer.GetComponent<CharacterManager>().CharacterController.Perspective,
                    Task = CurrentTask,
                    TaskNumber = GameManager.GetTaskNumber(CurrentTask),
                    Health = ElyPlayer.GetComponent<CharacterManager>().CharacterHandler.healthSystem.GetHealthPercent(),
                    Deaths = ElyPlayer.GetComponent<CharacterManager>().CharacterHandler.Deaths,
                    RuneCode = _levelBuilder.GetComponentInChildren<PuzzleRoomController>().GetCorrectKeyCodeAsString(),
                    CountWrongRunes = _levelBuilder.GetComponentInChildren<PuzzleRoomController>().CountWrongCodes,
                    Discovered = _levelBuilder.GetComponentInChildren<StealthObservator>().Discovered,
                    CountPaused = _countPaused,
                    Paused = Paused,

                    CombatSolved = _combatSolved,
                    DexSolved = _dexSolved,
                    PuzzleSolved = _puzzleSolved,
                    StealthSolved = _stealthSolved
                });
            }
        }

        //Todo
        if(Pausable)
        {
            if (!Paused)
            {
                if (Keyboard.current.escapeKey.wasPressedThisFrame)
                {
                    Paused = true;
                    PauseGame();
                    GameController.isGamePaused = true;
                    _countPaused++;
                }
            }
            else
            {
                if (Keyboard.current.escapeKey.wasPressedThisFrame)
                {
                    Paused = false;
                    ResumeGame();
                }
            }
        }
        CheckTriggers();
    }

    /// <summary>
    /// Starts the logging level data in the defined logging interval.
    /// </summary>
    public void StartLogging()
    {
        _startLogging = true;
    }

    /// <summary>
    /// Stops the logging procedure and resets the timer.
    /// </summary>
    public void StopLogging()
    {
        _startLogging = false;
    }

    /// <summary>
    /// Generates the level and the player.
    /// </summary>
    /// <param name="param">Condition for this level</param>
    internal void GenerateLevel(LevelParameter param)
    {
        _levelBuilder.order[0] = param.FirstTask;
        _levelBuilder.order[1] = param.SecondTask;
        _levelBuilder.order[2] = param.ThirdTask;
        _levelBuilder.order[3] = param.ForthTask;

        _levelBuilder.StartRoomGenereation();

        EndTrigger = _levelBuilder.EndRoom.GetComponentInChildren<ReachEnd>();
        PortalTrigger = _levelBuilder.EndRoom.GetComponentInChildren<PortalTriggerObservator>();
        EndCamera = _levelBuilder.EndRoom.transform.Find("EndCamera").GetComponent<CinemachineVirtualCamera>();

        _tutorial = _levelBuilder.StartRoom.GetComponentInChildren<TutorialController>();

        if (param.CameraCondition == ConditionPerpective.Upper)
        {
            _tutorial.GetComponent<TutorialController>().Floor = true;
        }
        else
        {
            _tutorial.GetComponent<TutorialController>().Floor = false;
        }


        SetupPlayer(param);
    }

    /// <summary>
    /// Starts the level.
    /// </summary>
    internal void StartLevel()
    {
        GameManager.FreeInput();
        ElyPlayer.GetComponent<CharacterManager>().CameraController.StartToPov();
        ElyPlayer.transform.Find("Canvas").gameObject.SetActive(true);

    }

    /// <summary>
    /// Activates the questionaire and focuses the player camera to it.
    /// </summary>
    internal void ShowQuestionaire()
    {
        GameManager.LockInput();
        ElyPlayer.transform.Find("Canvas").gameObject.SetActive(false);
        _questionaireController.ShuffleScales();
        _questionaireController.gameObject.SetActive(true);
        EndCamera.Priority = 100;
        Cursor.lockState = CursorLockMode.None;
    }

    /// <summary>
    /// Deactivates the questionaire and focuses the player camera back to its own pov.
    /// </summary>
    internal void HideQuestionaire()
    {
        GameManager.FreeInput();
        var canvas = ElyPlayer.transform.Find("Canvas").gameObject;
        canvas.SetActive(true);
        canvas.transform.Find("InteractableInformation").gameObject.SetActive(true);
        canvas.transform.Find("LowerText").gameObject.SetActive(true);
        _questionaireController.Reset();
        _questionaireController.gameObject.SetActive(false);
        EndCamera.Priority = 0;
        Cursor.lockState = CursorLockMode.Locked;
    }

    /// <summary>
    /// Shows the pause mneu and stops the game.
    /// </summary>
    internal void PauseGame()
    {
        GameManager.LockInput();
        _pauseMenue.PreviousCursorState = Cursor.lockState;
        Cursor.lockState = CursorLockMode.None;
        _pauseMenue.gameObject.SetActive(true);
        GameManager.PauseGameSpeed();
    }

    /// <summary>
    /// Deactivates the pause emnu and resumes the game.
    /// </summary>
    internal void ResumeGame()
    {
        GameManager.FreeInput();
        Cursor.lockState = _pauseMenue.PreviousCursorState;
        _pauseMenue.gameObject.SetActive(false);
        Paused = false;
        GameController.isGamePaused = false;
        GameManager.ResumeGameSpeed();
    }

    private void SetupPlayer(LevelParameter param)
    {
        ElyPlayer = Instantiate(ElyPlayerPrefab, new Vector3(-10, 11, -35), Quaternion.identity);
        switch (param.CameraCondition)
        {
            case ConditionPerpective.First:
                ElyPlayer.GetComponent<CharacterManager>().CharacterController.Perspective = CameraPerspective.First;
                ElyPlayer.GetComponent<CharacterManager>().CharacterController.LockPerspective = true;
                _tutorial.HideCameraChange = true;
                _tutorial.HideLean = false;
                FirstPopup = true;
                break;
            case ConditionPerpective.Third:
                ElyPlayer.GetComponent<CharacterManager>().CharacterController.Perspective = CameraPerspective.Third;
                ElyPlayer.GetComponent<CharacterManager>().CharacterController.LockPerspective = true;
                _tutorial.HideCameraChange = true;
                _tutorial.HideLean = false;
                FirstPopup = true;
                break;
            case ConditionPerpective.Upper:
                ElyPlayer.GetComponent<CharacterManager>().CharacterController.Perspective = CameraPerspective.UpperThird;
                ElyPlayer.GetComponent<CharacterManager>().CharacterController.LockPerspective = true;
                _tutorial.HideCameraChange = true;
                _tutorial.HideLean = true;
                FirstPopup = true;
                break;
            case ConditionPerpective.Free:
                ElyPlayer.GetComponent<CharacterManager>().CharacterController.Perspective = CameraPerspective.Third;
                ElyPlayer.GetComponent<CharacterManager>().CharacterController.LockPerspective = false;
                _tutorial.HideCameraChange = false;
                _tutorial.HideLean = false;
                FirstPopup = false;
                break;
        }
        ElyPlayer.GetComponent<CharacterManager>().CharacterController.GameManager = GameManager;
        ElyPlayer.GetComponent<CharacterManager>().CameraController.Controller = GameManager;
        ElyPlayer.GetComponent<CharacterManager>().CameraController.Callback += ShowPopupOnStart;
        ElyPlayer.GetComponent<CharacterManager>().PopUp.Callback += HidePopUp;
        ElyPlayer.GetComponent<CharacterManager>().CameraController.PostProcessing = _characterPostProcessing;
        SetupInspectionCamera();
    }

    private void ShowPopupOnStart()
    {
        if (FirstPopup) return;
        Pausable = false;
        FirstPopup = true;
        GameManager.LockInput();
        var PopupText = ElyPlayer.transform.Find("Canvas").transform.Find("InformCameraChange").gameObject;
        PopupText.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }

    private void HidePopUp()
    {
        Pausable = true;
        GameManager.FreeInput();
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void CheckTriggers()
    {
        var puzzleTriggerEntrance = _levelBuilder.PuzzleRoom.GetComponentInChildren<CloseRoomEntrance>();
        var combatTriggerEntrance = _levelBuilder.CombatRoom.GetComponentInChildren<CloseRoomEntrance>();
        var dexTriggerEntrance = _levelBuilder.DexRoom.GetComponentInChildren<CloseRoomEntrance>();
        var stealthTriggerEntrance = _levelBuilder.StealthRoom.GetComponentInChildren<CloseRoomEntrance>();

        var puzzleTriggerExit = _levelBuilder.PuzzleRoom.GetComponentInChildren<RoomExit>();
        var combatTriggerExit = _levelBuilder.CombatRoom.GetComponentInChildren<RoomExit>();
        var dexTriggerExit = _levelBuilder.DexRoom.GetComponentInChildren<RoomExit>();
        var stealthTriggerExit= _levelBuilder.StealthRoom.GetComponentInChildren<RoomExit>();

        if (puzzleTriggerEntrance.Triggered)
        {
            puzzleTriggerEntrance.Triggered = false;
            puzzleTriggerEntrance.enabled = false;
            CurrentTask = TaskType.Puzzle;
        }
        if (combatTriggerEntrance.Triggered)
        {
            combatTriggerEntrance.Triggered = false;
            combatTriggerEntrance.enabled = false;
            CurrentTask = TaskType.Combat;
        }
        if (dexTriggerEntrance.Triggered)
        {
            dexTriggerEntrance.Triggered = false;
            dexTriggerEntrance.enabled = false;
            CurrentTask = TaskType.Dexterity;
        }
        if (stealthTriggerEntrance.Triggered)
        {
            stealthTriggerEntrance.Triggered = false;
            stealthTriggerEntrance.enabled = false;
            CurrentTask = TaskType.Stealth;
            ElyPlayer.GetComponent<CharacterManager>().CharacterController.ForceSneak = true;
        }

        if (puzzleTriggerExit.Triggered)
        {
            puzzleTriggerExit.Triggered = false;
            puzzleTriggerExit.enabled = false;
            _puzzleSolved = true;
            GoldenKeyStatus.playerHasGoldenKey = false;
        }
        if (combatTriggerExit.Triggered)
        {
            combatTriggerExit.Triggered = false;
            combatTriggerExit.enabled = false;
            _combatSolved = true;
        }
        if (dexTriggerExit.Triggered)
        {
            dexTriggerExit.Triggered = false;
            dexTriggerExit.enabled = false;
            _dexSolved = true;
        }
        if (stealthTriggerExit.Triggered)
        {
            stealthTriggerExit.Triggered = false;
            stealthTriggerExit.enabled = false;
            _stealthSolved = true;
            ElyPlayer.GetComponent<CharacterManager>().CharacterController.ForceSneak = false;
        }

        if (EndTrigger != null)
        {
            if (EndTrigger.Triggered)
            {
                ShowQuestionaire();
                EndTrigger = null;
            }
        }
        if (PortalTrigger != null)
        {
            if (PortalTrigger.Triggered)
            {
                FinishedLevel = true;
                GameManager.LockInput();
            }
        }
    }

    private void SetupInspectionCamera()
    {
        if (!ElyPlayer) return;

        var overlayCamera = _levelBuilder.GetComponentInChildren<PuzzleRoomController>().transform.gameObject.transform.parent.Find("InspectionUI").GetComponentInChildren<Camera>();
        _inspectionUI = overlayCamera.transform.gameObject;
        _inspectionUI = _levelBuilder.GetComponentInChildren<PuzzleRoomController>().transform.gameObject.transform.parent.Find("InspectionUI").transform.gameObject;

        var mainCamera = ElyPlayer.GetComponentInChildren<CharacterManager>().CameraController.MainCamera;
        var cameraData = mainCamera.GetUniversalAdditionalCameraData();
        cameraData.cameraStack.Add(overlayCamera);

        var thirdPersonController = ElyPlayer.GetComponentInChildren<CharacterManager>().CharacterController;
        _levelBuilder.GetComponentInChildren<PuzzleRoomController>().gameController = thirdPersonController;
        _inspectionUI.gameObject.SetActive(false);
    }

}

internal class LevelParameter
{
    internal ConditionPerpective CameraCondition;
    internal TaskType FirstTask;
    internal TaskType SecondTask;
    internal TaskType ThirdTask;
    internal TaskType ForthTask;
}

using UnityEngine;

public class CloseRoomEntrance : MonoBehaviour
{
    /// <summary>
    /// The entrance gameObject that should be enabled to block the way back (e.g. a wall or a door).
    /// </summary>
    [SerializeField] 
    private GameObject entranceObject;

    internal bool Triggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Triggered = true;
            entranceObject?.SetActive(true);
        }
    }
    
}

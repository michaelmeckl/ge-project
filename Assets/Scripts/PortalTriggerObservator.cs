using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTriggerObservator : MonoBehaviour
{
    internal bool Triggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals("Player"))
        {
            Triggered = true;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LatinSquare
{
    public static Queue<ConditionPerpective> PerspectiveOrder;
    public static Queue<TaskType> TaskOrder;

    public static void CalculateLatinSquareByID(int id)
    {
        PerspectiveOrder = new Queue<ConditionPerpective>();
        TaskOrder = new Queue<TaskType>();

        var perspectiveDir = new Dictionary<int, ConditionPerpective>
        {
            {0, ConditionPerpective.First },
            {1, ConditionPerpective.Third },
            {2, ConditionPerpective.Upper },
            {3, ConditionPerpective.Free }
        };

        var taskDir = new Dictionary<int, TaskType>
        {
            {0, TaskType.Puzzle },
            {1, TaskType.Combat },
            {2, TaskType.Dexterity },
            {3, TaskType.Stealth }
        };

        var mod = id % 4;
        Debug.Log(mod);

        for (int i = 0; i < 4; i++)
        {
            PerspectiveOrder.Enqueue(perspectiveDir[(mod + i) % 4]);
            TaskOrder.Enqueue(taskDir[(mod + i) % 4]);
        }
    }

    public static void PrintLatinSquare()
    {
        Debug.Log("Perspective Order: ");
        foreach(var e in PerspectiveOrder)
        {
            Debug.Log(e.ToString());
        }
        Debug.Log("-----");
        Debug.Log("Task Order: ");
        foreach (var e in TaskOrder)
        {
            Debug.Log(e.ToString());
        }
    }
}


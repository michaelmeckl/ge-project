using System;
using System.IO;
using System.Linq;
using UnityEngine;

public static class LoggingSystem
{
    public static readonly string Now = DateTime.UtcNow.AddHours(1).ToString().Replace(' ', '_').Replace('.', '-').Replace(':', '-');
    public static readonly string FilePath = "StudyLogs";
    public static string FileNameMain = "log.csv";
    public static string FileNameIMI = "imi.csv";
    public static string FileNamePIX = "pxi.csv";

    public static string ParticipantID = "X";

    public static string FullFilePathMain
    {
        get => FilePath + "/" + ParticipantID + "_" + Now + "_" + FileNameMain;
    }

    public static string FullFilePathIMI
    {
        get => FilePath + "/" + ParticipantID + "_" + Now + "_" + FileNameIMI;
    }

    public static string FullFilePathPIX
    {
        get => FilePath + "/" + ParticipantID + "_" + Now + "_" + FileNamePIX;
    }

    public static void CreateDirectory()
    {
        if (!Directory.Exists(FilePath))
        {
            Debug.Log("Didn't find logging directory. Creating ...");
            Directory.CreateDirectory(FilePath);
        }
        else
        {
            Debug.Log("Logging directory found. Using directory as standard logging directory ...");
        }
    }

    public static bool CreateFile(LogFile type)
    {
        CreateDirectory();

        switch (type)
        {
            case LogFile.Main:
                if (!File.Exists(FullFilePathMain))
                {
                    Debug.Log("Creating logging file " + FullFilePathMain);
                    File.Create(FullFilePathMain).Close();
                    return true;
                }
                break;
            case LogFile.Imi:
                if (!File.Exists(FullFilePathIMI))
                {
                    Debug.Log("Creating logging file " + FullFilePathIMI);
                    File.Create(FullFilePathIMI).Close();
                    return true;
                }
                break;
            case LogFile.Pix:
                if (!File.Exists(FullFilePathPIX))
                {
                    Debug.Log("Creating logging file " + FullFilePathPIX);
                    File.Create(FullFilePathPIX).Close();
                    return true;
                }
                break;
        }
        return false;
    }

    public static void CreateCSVHeader(LogFile type)
    {
        switch (type)
        {
            case LogFile.Main:
                if (!CreateFile(type))
                {
                    Debug.LogWarning("Logging file " + FullFilePathMain + " already found! Clearing it to create empty file with appropiate header ...");
                    File.WriteAllText(FullFilePathMain, String.Empty);
                }
                File.AppendAllText(FullFilePathMain, typeof(LogDataSet).GetFields().Select(x => x.Name).ToList().Aggregate((x, y) => x + "; " + y) + "\n");
                break;
            case LogFile.Imi:
                if (!CreateFile(type))
                {
                    Debug.LogWarning("Logging file " + FullFilePathIMI + " already found! Clearing it to create empty file with appropiate header ...");
                    File.WriteAllText(FullFilePathIMI, String.Empty);
                }
                File.AppendAllText(FullFilePathIMI, typeof(IMIDataSet).GetFields().Select(x => x.Name).ToList().Aggregate((x, y) => x + "; " + y) + "\n");
                break;
            case LogFile.Pix:
                if (!CreateFile(type))
                {
                    Debug.LogWarning("Logging file " + FullFilePathPIX + " already found! Clearing it to create empty file with appropiate header ...");
                    File.WriteAllText(FullFilePathPIX, String.Empty);
                }
                File.AppendAllText(FullFilePathPIX, typeof(PIXDataSet).GetFields().Select(x => x.Name).ToList().Aggregate((x, y) => x + "; " + y) + "\n");
                break;
        }
    }

    public static void CreateCSVHeader(LogFile type, string header)
    {
        switch (type)
        {
            case LogFile.Main:
                if (!CreateFile(type))
                {
                    Debug.LogWarning("Logging file " + FullFilePathMain + " already found! Clearing it to create empty file with appropiate header ...");
                    File.WriteAllText(FullFilePathMain, String.Empty);
                }
                File.AppendAllText(FullFilePathMain, header);
                break;
            case LogFile.Imi:
                if (!CreateFile(type))
                {
                    Debug.LogWarning("Logging file " + FullFilePathIMI + " already found! Clearing it to create empty file with appropiate header ...");
                    File.WriteAllText(FullFilePathIMI, String.Empty);
                }
                File.AppendAllText(FullFilePathIMI, header);
                break;
            case LogFile.Pix:
                if (!CreateFile(type))
                {
                    Debug.LogWarning("Logging file " + FullFilePathPIX + " already found! Clearing it to create empty file with appropiate header ...");
                    File.WriteAllText(FullFilePathPIX, String.Empty);
                }
                File.AppendAllText(FullFilePathPIX, header);
                break;
        }
    }

    [Obsolete]
    public static bool ExistingFileAndWriteable(LogFile type)
    {
        return true;
        /*
        switch (type)
        {
            case LogFile.Main:
                if (!CreateFile(type))
                {
                    return false;
                }
                break;
            case LogFile.Imi:
                if (!CreateFile(type))
                {
                    return false;
                }
                break;
            case LogFile.Pix:
                if (!CreateFile(type))
                {
                    return false;
                }
                break;
        }
        return true;*/
    }

    public static void AppendDataSet(LogDataSet set)
    {
        if(ExistingFileAndWriteable(LogFile.Main))
        {
            File.AppendAllText(FullFilePathMain, set.TimeStampAsCSV + "\n");
        }
        else
        {
            Debug.LogError("Logging file not properly formated. Delete and create again to write data sets to it.");
        }
    }

    public static void AppendImiSet(IMIDataSet set)
    {
        if (ExistingFileAndWriteable(LogFile.Imi))
        {
            File.AppendAllText(FullFilePathIMI, set.DataSetAsCSV + "\n");
        }
        else
        {
            Debug.LogError("Logging file not properly formated. Delete and create again to write data sets to it.");
        }
    }

    public static void AppendPixSet(PIXDataSet set)
    {
        if (ExistingFileAndWriteable(LogFile.Pix))
        {
            File.AppendAllText(FullFilePathPIX, set.DataSetAsCSV + "\n");
        }
        else
        {
            Debug.LogError("Logging file not properly formated. Delete and create again to write data sets to it.");
        }
    }

}

public enum LogFile
{
    Main,
    Imi,
    Pix
}

public class LogDataSet
{
    public DateTime TimeStamp;
    public long TimeStampUnixTime;
    public int ID;
    public float Health;
    public int LevelNumber;
    public ConditionPerpective ConditionCamera;
    public int TaskNumber;
    public TaskType Task;
    public CameraPerspective UsedPerspective;
    public int Deaths;
    public string RuneCode;
    public int CountWrongRunes;
    public bool Discovered;

    public int CountPaused;
    public bool Paused;

    public bool PuzzleSolved;
    public bool CombatSolved;
    public bool DexSolved;
    public bool StealthSolved;

    public String TimeStampAsCSV
    {
        get
        {
            return TimeStamp.ToString() + "; " +
                TimeStampUnixTime.ToString() + "; " +
                ID.ToString() + "; " +
                Health.ToString() + "; " +
                LevelNumber.ToString() + "; " +
                ConditionCamera.ToString() + "; " +
                TaskNumber.ToString() + "; " +
                Task.ToString() + "; " +
                UsedPerspective.ToString() + "; " +
                Deaths.ToString() + "; " +
                RuneCode.ToString() + "; " +
                CountWrongRunes.ToString() + "; " +
                Discovered.ToString() + "; " +

                CountPaused.ToString() + "; " +
                Paused.ToString() + "; " +

                PuzzleSolved.ToString() + "; " +
                CombatSolved.ToString() + "; " +
                DexSolved.ToString() + "; " +
                StealthSolved.ToString();

        }
    }

    public void PrintDataSetToConsole()
    {
        Debug.Log(TimeStampAsCSV);
    }
}

public class IMIDataSet
{
    public DateTime TimeStamp;
    public long TimeStampUnixTime;
    public int ID;

    public ConditionPerpective ConditionCamera;
    public int LevelNumber;

    public int EnjoymentItem1;
    public int EnjoymentItem2;
    public int EnjoymentItem3;
    public int EnjoymentItem4;
    public int EnjoymentItem5;
    public int EnjoymentItem6;
    public int EnjoymentItem7;

    public int CompetenceItem1;
    public int CompetenceItem2;
    public int CompetenceItem3;
    public int CompetenceItem4;
    public int CompetenceItem5;
    public int CompetenceItem6;

    public String DataSetAsCSV
    {
        get
        {
            return TimeStamp.ToString() + "; " +
                TimeStampUnixTime.ToString() + "; " +
                ID.ToString() + "; " +
                ConditionCamera.ToString() + "; " +
                LevelNumber.ToString() + "; " +

                EnjoymentItem1.ToString() + "; " +
                EnjoymentItem2.ToString() + "; " +
                EnjoymentItem3.ToString() + "; " +
                EnjoymentItem4.ToString() + "; " +
                EnjoymentItem5.ToString() + "; " +
                EnjoymentItem6.ToString() + "; " +
                EnjoymentItem7.ToString() + "; " +

                CompetenceItem1.ToString() + "; " +
                CompetenceItem2.ToString() + "; " +
                CompetenceItem3.ToString() + "; " +
                CompetenceItem4.ToString() + "; " +
                CompetenceItem5.ToString() + "; " +
                CompetenceItem6.ToString();
        }
    }

    public void PrintDataSetToConsole()
    {
        Debug.Log(DataSetAsCSV);
    }
}

public class PIXDataSet
{
    public DateTime TimeStamp;
    public long TimeStampUnixTime;
    public int ID;

    public ConditionPerpective ConditionCamera;
    public int LevelNumber;

    public int AutonomyItem1;
    public int AutonomyItem2;
    public int AutonomyItem3;

    public int ImmersionItem1;
    public int ImmersionItem2;
    public int ImmersionItem3;


    public String DataSetAsCSV
    {
        get
        {
            return TimeStamp.ToString() + "; " +
                TimeStampUnixTime.ToString() + "; " +
                ID.ToString() + "; " +
                ConditionCamera.ToString() + "; " +
                LevelNumber.ToString() + "; " +

                AutonomyItem1.ToString() + "; " +
                AutonomyItem2.ToString() + "; " +
                AutonomyItem3.ToString() + "; " +

                ImmersionItem1.ToString() + "; " +
                ImmersionItem2.ToString() + "; " +
                ImmersionItem3.ToString();
        }
    }

    public void PrintDataSetToConsole()
    {
        Debug.Log(DataSetAsCSV);
    }
}

public enum TaskType
{
    None,
    Combat,
    Stealth,
    Puzzle,
    Dexterity
}

public enum ConditionPerpective
{
    First,
    Third,
    Upper,
    Free
}

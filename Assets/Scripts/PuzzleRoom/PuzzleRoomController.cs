using System;
using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PuzzleRoomController : MonoBehaviour
{
    
    public ThirdPersonController gameController;

    [SerializeField] 
    private GenerateRandomRunes runeGenerator;
    
    [SerializeField]
    private GameObject inspectionUI;
    
    [SerializeField]
    private Image interactableInformationText;
    
    [SerializeField] 
    private GameObject interactButton;
    
    [SerializeField] 
    private TMP_InputField keyCodeInput;

    [SerializeField]
    private GameObject inspectionObjectInfoScroll;
    
    [SerializeField]
    private GameObject inspectionObjectDisplayCaseLock;
    
    [SerializeField]
    private GameObject inspectionObjectSteampunkBook;
    
    [SerializeField]
    private GameObject inspectionObjectScrollWithRune;

    
    private List<GameObject> allInspectionObjects = new();

    private bool isShowingInspectUI = false;

    private List<int> runeKeyCode;

    internal int CountWrongCodes;
    
    
    // internal delegate void OnInspectViewExit();
    // internal OnInspectViewExit inspectViewExitEvent;
    
    internal delegate void OnKeyAcquired();
    internal OnKeyAcquired keyAcquiredEvent;
    
    private void Awake()
    {
        allInspectionObjects = new List<GameObject>
        {
            inspectionObjectInfoScroll, inspectionObjectDisplayCaseLock, inspectionObjectSteampunkBook, inspectionObjectScrollWithRune
        };
        
        // must be called in Awake so the actual delegate will be called later in 'GenerateRandomRunes' OnStart() Method
        runeGenerator.runeCodeGenerated += SetRuneKeyCode;
        CountWrongCodes = 0;
    }

    private void OnEnable()
    {
        // register two listeners so the correct keycode is automatically accepted after the last digit is entered but the
        // wrong code message is also only shown once at the end and the inputField text is reset when leaving or entering the wrong code
        keyCodeInput.onValueChanged.AddListener(delegate{OnKeyCodeChanged(keyCodeInput);});
        keyCodeInput.onEndEdit.AddListener(delegate{OnEnteredKeyCode(keyCodeInput);});
    }

    private void OnDisable()
    {
        keyCodeInput.onValueChanged.RemoveListener(delegate{OnKeyCodeChanged(keyCodeInput);});
        keyCodeInput.onEndEdit.RemoveListener(delegate{OnEnteredKeyCode(keyCodeInput);});
    }

    private void OnKeyCodeChanged(TMP_InputField inputField)
    {
        var enteredKey = inputField.text;
        var correctKey = string.Join("", runeKeyCode);

        if (enteredKey == correctKey)
        {
            OnCorrectKeyCodeEntered();
        }
    }
    

    private void OnEnteredKeyCode(TMP_InputField inputField)
    {
        var enteredKey = inputField.text;
        var correctKey = string.Join("", runeKeyCode);

        if (enteredKey == correctKey)
        {
            OnCorrectKeyCodeEntered();
        }
        else
        {
          
            // print($"Entered key {enteredKey} was not correct! (Correct key: {correctKey}");
            SetInformationText("Falscher Code!");
			CountWrongCodes++;
            inputField.text = "";  // reset input field if code was wrong
        }
    }

    private void OnCorrectKeyCodeEntered()
    {
        GoldenKeyStatus.playerHasGoldenKey = true;
            
        // close inspection window
        ToggleInspectionUIVisibility(isVisible: false);
        LockGameInput(false);
            
        SetInformationText("Korrekt! Du hast den \"Goldenen Schlüssel\" erhalten!", duration: 3f);

        keyAcquiredEvent();  // notify listeners (this sets the golden key and the combination lock to inactive)
            
        // also set the trigger for the lock to inactive
        var lockedDisplayCaseTrigger = transform.Find("InteractTrigger_DisplayCase");
        lockedDisplayCaseTrigger.gameObject.SetActive(false);
    }
    
    private void SetRuneKeyCode(List<int> keyCode, string firstRune)
    {
        runeKeyCode = keyCode;
        
        //print("RuneCode is: \n");
        //runeKeyCode.ForEach(k => print(k));
    }
    
    private void Update()
    {
        if (isShowingInspectUI && Mouse.current.rightButton.wasPressedThisFrame)
        {
            ToggleInspectionUIVisibility(isVisible: false);
            LockGameInput(false);
        }
    }

    internal string GetCorrectKeyCodeAsString()
    {
        return string.Join("", runeKeyCode);
    }

    public void SetInformationText(string infoText, float duration = 2f)
    {
        StartCoroutine(ShowMessage(infoText, duration));
    }

    private IEnumerator ShowMessage(string newText, float duration)
    {
        var tmpObject = interactableInformationText.transform.GetChild(0).gameObject;
        tmpObject.GetComponent<TextMeshProUGUI>().text = newText;
        
        interactableInformationText.gameObject.SetActive(true);
        
        yield return new WaitForSeconds(duration);
        interactableInformationText.gameObject.SetActive(false);
    }

    public List<GameObject> GetAllInteractables()
    {
        return allInspectionObjects;
    }

    public void LockGameInput(bool lockInput)
    {
        if (lockInput)
        {
            gameController.GameManager.LockInput();
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            gameController.GameManager.FreeInput();
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    public void ToggleInspectionUIVisibility(bool isVisible)
    {
        inspectionUI.SetActive(isVisible);
        isShowingInspectUI = isVisible;
    }
    
    public void ToggleInteractButtonVisibility(bool isVisible)
    {
        interactButton.SetActive(isVisible);
    }
    
    
}
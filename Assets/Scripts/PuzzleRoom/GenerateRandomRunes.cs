using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;


public class GenerateRandomRunes : MonoBehaviour
{
    /// <summary>
    /// The pool of runes from which 3 should be randomly picked.
    /// </summary>
    [SerializeField] 
    private List<GameObject> availableRunes;

    /// <summary>
    /// The fixed positions where the 3 runes should be placed in the level.
    /// </summary>
    [SerializeField]
    private List<GameObject> runePositions;
    
    // maps the rune names to the corresponding number in the alphabet
    private Dictionary<string, int> _runeMapping = new Dictionary<string, int>
    {
        ["Runes-A"] = 1,
        ["Runes-D"] = 4,
        ["Runes-F"] = 6,
        ["Runes-S"] = 19,
        ["Runes-T"] = 20,
        ["Runes-Y"] = 25,
        ["Runes-Z"] = 26,
    };
    
    
    internal delegate void OnRuneCodeGenerated(List<int> keyCode, string firstRune);
    internal OnRuneCodeGenerated runeCodeGenerated;

    private void Start()
    {
        // set a random seed:
        // Random.InitState(420);
        
        GenerateRunes();
    }

    private void GenerateRunes()
    {
        // pick 3 runes randomly and instantiate each of them in one of the given rune positions
        var chosenRunes = GetRandomItemsFromList(availableRunes);
        GenerateKeycode(chosenRunes);

        // the first rune is used on two positions (one for top-down and one for first & third person) so we need to add the first rune again
        chosenRunes.Insert(0, chosenRunes[0]);

        for (int i = 0; i < chosenRunes.Count; i++)
        {
            var rune = chosenRunes[i];
            var runePosition = runePositions[i].transform;   // get the transform of the corresponding placeholder rune
            var instantiatedRune = Instantiate(rune, runePosition.localPosition, runePosition.localRotation);
            
            // also set the localScale of the placeholder rune so any size changes are kept
            instantiatedRune.transform.localScale = runePosition.transform.localScale;
            
            // check if the rune placeholder has a parent so we can set the new instance at the same position
            var runePositionParent = runePosition.parent;
            if (runePositionParent != null)
            {
                // Important: woldPositionStays must be set to false so the correct position and scale of the placeholder is maintained!
                instantiatedRune.transform.SetParent(runePositionParent, false);
            }

            // Make sure to keep the layer of the placeholder as well as otherwise the runes placed in the UI canvas wouldn't be shown!
            var oldLayer = LayerMask.LayerToName(runePosition.gameObject.layer);
            instantiatedRune.gameObject.layer = LayerMask.NameToLayer(oldLayer);

            // important to set the child to the corresponding layer as well!
            var childObj = instantiatedRune.transform.GetChild(0);
            childObj.gameObject.layer = LayerMask.NameToLayer(oldLayer);
        }
        
        // Destroy all placeholders so they won't be activated and shown in the inspection ui as well
        runePositions.ForEach(Destroy);
    }

    private List<GameObject> GetRandomItemsFromList(in List<GameObject> items, int numberOfItems = 3)
    {
        // make a copy of the list to be able to edit it locally without modifying the original list
        var itemsCopy = items.Select(go => go).ToList();
        var chosenItems = new List<GameObject>();

        for (int i = 0; i < numberOfItems; i++)
        {
            if (itemsCopy.Count > 0)
            {
                // get a random item
                int randomIndex = Random.Range(0, itemsCopy.Count);
                GameObject randomListItem = itemsCopy[randomIndex];
            
                // remove the chosen item so it won't be chosen again
                itemsCopy.Remove(itemsCopy[randomIndex]);
                chosenItems.Add(randomListItem);
            }
        }

        return chosenItems;
    }

    private void GenerateKeycode(List<GameObject> chosenRunes)
    {
        var runeCode = new List<int>();
        
        foreach (var rune in chosenRunes)
        {
            var runeKey = _runeMapping[rune.name];
            runeCode.Add(runeKey);
        }
        
        runeCodeGenerated.Invoke(runeCode, chosenRunes[0].name);
    }
}

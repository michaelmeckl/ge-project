using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

/// NOT USED
public class RandomisePuzzleRoom : MonoBehaviour
{
    // [SerializeField]
    // private List<GameObject> roomEntrancePositions;

    /// <summary>
    /// The rooms that should be randomized, i.e. the room positions are randomly shuffled.
    /// </summary>
    [SerializeField]
    private List<GameObject> rooms;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        print("Starting to randomise puzzle room layout ...");

        var startTime = Time.realtimeSinceStartup;;
        SwapRooms();
        
        print($"Finished generating random puzzle room layout in {(Time.realtimeSinceStartup - startTime) * 1000} ms !");
    }

    
    private void SwapRooms()
    {
        Util.Shuffle(rooms);   // random shuffle the room order

        GameObject lastRoom;
        for (int i = 1; i < rooms.Count; i++)
        {
            lastRoom = rooms[i - 1];
            var currentRoom = rooms[i];
            
            // swap room positions
            (currentRoom.transform.localPosition, lastRoom.transform.localPosition) = (lastRoom.transform.localPosition, currentRoom.transform.localPosition);
            (currentRoom.transform.localRotation, lastRoom.transform.localRotation) = (lastRoom.transform.localRotation, currentRoom.transform.localRotation);
        }
    }
    
    private void GenerateRoomLayout()
    {
        var roomListCopy = new List<GameObject>();
        roomListCopy.AddRange(rooms.Select(room => room).ToList());
        
        Util.Shuffle(roomListCopy);   // random shuffle the room order

        var parentRoom = rooms[0].transform.parent;
        
        var arrLength = roomListCopy.Count;
        for (int i = 0; i < arrLength; i++)
        {
            var room = rooms[i];
            var roomPosition = room.transform.position;
            var roomRotation = room.transform.rotation;

            // get a room from the shuffled list
            var newRoom = roomListCopy[i];

            print($"Replacing room {room} with new room {newRoom}");

            /*
            var parent = new GameObject("PivotParent");
            var pivotParent = Instantiate(parent, roomPosition, roomRotation);
            */
            
            Destroy(room);
            
            var instantiatedRoom = Instantiate(newRoom, roomPosition, roomRotation, parent: parentRoom);
            
            print($"Instantiate room {newRoom.name} at position {roomPosition} and rotation {roomRotation}");
        }
    }
    
    // Method taken from https://answers.unity.com/questions/486626/how-can-i-shuffle-alist.html
    [Obsolete]
    private void Shuffle<T>(List<T> inputList)
    {
        for (int i = 0; i < inputList.Count; i++)
        {
            T temp = inputList[i];
            int rand = Random.Range(i, inputList.Count);
            inputList[i] = inputList[rand];
            inputList[rand] = temp;
        }
    }
}
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TriggerRunePuzzleHelp : MonoBehaviour
{
    [SerializeField] 
    private GenerateRandomRunes runeGenerator;
    
    [SerializeField]
    private GameObject runeHelpTextField;

    private void Awake()
    {
        // must be called in Awake so the actual delegate will be called later in 'GenerateRandomRunes' OnStart() Method
        runeGenerator.runeCodeGenerated += SetCodeForFirstRune;
    }

    private void SetCodeForFirstRune(List<int> keyCode, string firstRuneIdentifier)
    {
        var firstRuneNumber = keyCode[0];
        var firstRune = firstRuneIdentifier.Split("-")[1];  // "Runes-T" to "T"
        
        var tmpTextObject = runeHelpTextField.transform.GetChild(0).gameObject;
        // tmpTextObject.GetComponent<TextMeshProUGUI>().text = $"Sieht wie ein {firstRune} aus ... Also eine {firstRuneNumber}?";
        tmpTextObject.GetComponent<TextMeshProUGUI>().text = $"Hm, könnte diese Rune dann einer {firstRuneNumber} entsprechen?";
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // show the rune help info text
            runeHelpTextField.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // hide the rune info text again
            runeHelpTextField.SetActive(false);
        }
    }
}
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class InteractWithObject : MonoBehaviour
{
    [SerializeField] 
    private PuzzleRoomController _puzzleRoomController;
    
    [SerializeField]
    private GameObject interactable;

    private bool interactionAvailable = false;

    private void Start()
    {
        _puzzleRoomController.keyAcquiredEvent += OnGoldenKeyAcquired;
    }

    private void Update()
    {
        var fKeyPressed = Keyboard.current.fKey.wasPressedThisFrame;

        switch (fKeyPressed)
        {
            case true when interactionAvailable:
                // TODO should probably be done with raycasts instead so there can be two interactables at the same time
                //      but this works just fine for now ¯\_(ツ)_/¯
                //print("Interacted with object " + interactable.name);
                OnInteractionTriggered();
                break;
            case true when !interactionAvailable:
                //print("Not in interaction range for object: " + interactable.name);
                break;
        }
    }

    private void OnInteractionTriggered()
    {
        
        switch (interactable.tag)
        {
            case "GoldenDoor":
                // check if golden key flag is already set (i.e. if the key was acquired) else show Locked note on screen
                if (GoldenKeyStatus.playerHasGoldenKey)
                {
                    _puzzleRoomController.SetInformationText("Glückwunsch, du hast das Rätsel gelöst!");
                    interactable.gameObject.SetActive(false);

                    // TODO this freezes the information text above; instead create a trigger in the corridor at the exit and disable the manager there
                    // disable the puzzleRoomController as it is not needed anymore
                    //_puzzleRoomController.gameObject.SetActive(false);
                }
                else
                {
                    _puzzleRoomController.SetInformationText("Die Tür ist fest verschlossen!");
                }
                break;

            case "LockedShowcase" :
                EnterInspectionMode("LockedShowcase");
                break;
            case "TextScroll":
                EnterInspectionMode("TextScroll");
                break;
            case "ScrollWithRune":
                EnterInspectionMode("ScrollWithRune");
                break;
            case "BookWithRune":
                EnterInspectionMode("BookWithRune");
                break;
        }
        
        // disable interaction button so it won't be shown in the inspection canvas
        _puzzleRoomController.ToggleInteractButtonVisibility(false);
        interactionAvailable = false;
    }
    
    private void EnterInspectionMode(string inspectionObjectTag)
    {
        _puzzleRoomController.LockGameInput(true);
        ToggleInspectionObjects(inspectionObjectTag);
        
        _puzzleRoomController.ToggleInspectionUIVisibility(isVisible: true);
        //print($"Entering inspection mode for gameobject: {interactable.name} (inspectionTag: {inspectionObjectTag})");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        _puzzleRoomController.ToggleInteractButtonVisibility(true);
        interactionAvailable = true;
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        _puzzleRoomController.ToggleInteractButtonVisibility(false);
        interactionAvailable = false;
    }
    
    private void ToggleInspectionObjects(String objectToShowTag)
    {
        var allInspectionObjects = _puzzleRoomController.GetAllInteractables();
        foreach (var inspectionObject in allInspectionObjects)
        {
            if (inspectionObject.CompareTag(objectToShowTag))
            {
                inspectionObject.SetActive(true);
            }
            else
            {
                inspectionObject.SetActive(false);
            }
        }
    }

    private void OnGoldenKeyAcquired()
    {
        // only react if the interactable in this script is the LockedDisplayCase
        if (!interactable.CompareTag("LockedShowcase")) return;

        var goldenKeyChild = interactable.transform.Find("Golden_Key");
        var combinationLockChild = interactable.transform.Find("Combination_lock");

        if (goldenKeyChild == null)
        {
            //Debug.LogWarning("Golden Key child is null in OnGoldenKeyAcquired");
            return;
        }
        
        goldenKeyChild.gameObject.SetActive(false);
        combinationLockChild.gameObject.SetActive(false);
    }
}
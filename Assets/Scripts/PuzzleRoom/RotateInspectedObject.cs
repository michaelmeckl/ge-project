using UnityEngine;
using UnityEngine.InputSystem;

// Code taken from https://www.youtube.com/watch?v=cu39TdzirCE
public class RotateInspectedObject : MonoBehaviour
{
    private Vector3 posLastFrame;

    private void Update()
    {
        Vector3 mousePos = Mouse.current.position.ReadValue();
       
        var delta = mousePos - posLastFrame;
        posLastFrame = mousePos;

        // rotate the object if the left mouse button is pressed
        bool leftMouseButtonIsPressed = Mouse.current.leftButton.isPressed;
        if (leftMouseButtonIsPressed)
        {
            var axis = Quaternion.AngleAxis(-90f, Vector3.forward) * delta;
            transform.rotation = Quaternion.AngleAxis(delta.magnitude * 0.1f, axis) * transform.rotation;  
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.Universal;

/// <summary>
/// Main Game Manager which contains global parameters like gamespeed and states.
/// </summary>
public class GameController : MonoBehaviour
{
    public static float MasterVolume = 0.1f;

    public static bool isGamePaused = false;
    
    internal LevelManager LevelManager;

    [SerializeField]
    private Questionaire _questionaire;

    private uint BackgroundMusicMenu;
    private uint BackgroundMusicGame;

    [Header("Parameters if builded")]
    public int ParticipantID = 0;

    /// <summary>
    /// Returns the current game speed in percent as float between 0f and 1f
    /// </summary>
    public float GameSpeed
    {
        get => _gameSpeed;
        private set
        {
            _gameSpeed = value;
        }
    }

    private float _gameSpeed;

    /// <summary>
    /// Returns if the game is currently paused (gamespeed <= 0).
    /// </summary>
    public bool Paused
    {
        get
        {
            return GameSpeed <= 0;
        }
    }

    internal delegate void OnGameSpeedZero();
    internal delegate void OnGameSpeedResume();

    /// <summary>
    /// Callback which gets invoked, once the gamespeed is set to zero from elsewhere in the game (e.g. paused)
    /// </summary>
    internal OnGameSpeedZero GameSpeedZeroCallback;
    /// <summary>
    /// Callback which gets invoked, once the gamespeed is set to 1 from elsewhere in the game (e.g. resumed)
    /// </summary>
    internal OnGameSpeedZero GamespeedResumeCallback;

    /// <summary>
    /// Returns if the game inputs are currently locked and are only recieving minimal inputs
    /// </summary>
    internal bool LockedInput
    {
        get => _lockedInput;
        private set
        {
            _lockedInput = value;
        }
    }

    private bool _lockedInput;

    internal delegate void OnInputLock();
    internal delegate void OnInputRelease();

    /// <summary>
    /// Callback which gets invoked, once the input is locked from elsewhere in the game.
    /// </summary>
    internal OnInputLock InputLockCallback;
    /// <summary>
    /// Callback which gets invoked, once the input is freed from elsewhere in the game.
    /// </summary>
    internal OnInputRelease InputReleaseCallback;

    /// <summary>
    /// 
    /// </summary>
    internal GameState CurrentGameState { get; private set; }

    //Internal Controls
    public BlackFadeController BlackFadeControl;
    public StartMenuObservator Menu;

    private CamCon _one;
    private CamCon _two;
    private CamCon _three;
    private CamCon _four;

    private TaskType _firstTask;
    private TaskType _secondTask;
    private TaskType _thirdTask;
    private TaskType _forthTask;

    private bool ChangingScene = false;

    /// <summary>
    /// Locks the input and invokes the callbacks.
    /// </summary>
    public void LockInput()
    {
        LockedInput = true;
        InputLockCallback?.Invoke();
    }

    /// <summary>
    /// Frees the input and invokes the callbacks.
    /// </summary>
    public void FreeInput()
    {
        LockedInput = false;
        InputReleaseCallback?.Invoke();
    }

    /// <summary>
    /// FSets the gamespeed to 0 and invokes the callbacks.
    /// </summary>
    public void PauseGameSpeed()
    {
        GameSpeed = 0f;
        GameSpeedZeroCallback?.Invoke();
    }

    /// <summary>
    /// FSets the gamespeed to 1 and invokes the callbacks.
    /// </summary>
    public void ResumeGameSpeed()
    {
        GameSpeed = 1f;
        GamespeedResumeCallback?.Invoke();
    }


    private void Awake()
    {
#if UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
#else
        Debug.unityLogger.logEnabled = false;
#endif
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        Menu = GameObject.Find("StartMenu").GetComponent<StartMenuObservator>();

        CurrentGameState = GameState.Start;
        GameSpeed = 1.0f;
        LockInput();

        Menu.StartButtonCallback += StartGame;

        SceneManager.sceneUnloaded += SceneUnloaded;
        SceneManager.sceneLoaded += SceneLoaded;

        BlackFadeControl.Reciever.FadeInCallback += CompleteFadeIn;
        BlackFadeControl.Reciever.FadeOutCallback += CompleteFadeOut;

        BlackFadeControl.Reciever.FadeIn(CurrentGameState);
    }

    // Update is called once per frame
    void Update()
    {
        switch (CurrentGameState)
        {
            case GameState.Start:
                ParticipantID = Menu.GetParticipantID();                
                break;
            case GameState.Level1:
                if (LevelManager.FinishedLevel && !ChangingScene)
                {
                    LevelManager.FinishedLevel = false;
                    ChangingScene = true;
                    LevelManager.StopLogging();
                    ChangeGameState(GameState.Level2);
                }
                break;
            case GameState.Level2:
                if (LevelManager.FinishedLevel && !ChangingScene)
                {
                    LevelManager.FinishedLevel = false;
                    ChangingScene = true;
                    LevelManager.StopLogging();
                    ChangeGameState(GameState.Level3);
                }
                break;
            case GameState.Level3:
                if (LevelManager.FinishedLevel && !ChangingScene)
                {
                    LevelManager.FinishedLevel = false;
                    ChangingScene = true;
                    LevelManager.StopLogging();
                    ChangeGameState(GameState.Level4);
                }
                break;
            case GameState.Level4:
                if (LevelManager.FinishedLevel && !ChangingScene)
                {
                    LevelManager.FinishedLevel = false;
                    ChangingScene = true;
                    LevelManager.StopLogging();
                    ChangeGameState(GameState.End);
                }
                break;
            case GameState.End:
                ParticipantID = Menu.GetParticipantID();
                break;
        }
    }

    private void CreateLogFiles(int id)
    {
        Debug.Log("Creating logfiles.");
        if(ParticipantID != id)
        {
            Debug.LogWarning("ID mismatch! Overwritting Participant ID from " + ParticipantID + " to " + id + ".");
            ParticipantID = id;
        }
        LoggingSystem.ParticipantID = ParticipantID.ToString();
        LoggingSystem.CreateCSVHeader(LogFile.Main);
        var ImiHeader = String.Empty;
        ImiHeader = typeof(IMIDataSet).GetFields().Where(x => !x.Name.Contains("Item")).Select(x => x.Name).ToList().Aggregate((x, y) => x + "; " + y);
        ImiHeader += "; " + _questionaire.ImiInterestData.Select(x => x.ItemData.Label.text).ToList().Aggregate((x, y) => x + "; " + y);
        ImiHeader += "; " + _questionaire.ImiCompetenceData.Select(x => x.ItemData.Label.text).ToList().Aggregate((x, y) => x + "; " + y);
        ImiHeader.Replace("\n", String.Empty);
        ImiHeader += "\n";
        LoggingSystem.CreateCSVHeader(LogFile.Imi, ImiHeader);
        var PixHeader = String.Empty;
        PixHeader = typeof(PIXDataSet).GetFields().Where(x => !x.Name.Contains("Item")).Select(x => x.Name).ToList().Aggregate((x, y) => x + "; " + y);
        PixHeader += "; " + _questionaire.PixAutonomyData.Select(x => x.ItemData.Label.text).ToList().Aggregate((x, y) => x + "; " + y);
        PixHeader += "; " + _questionaire.PixImmersionData.Select(x => x.ItemData.Label.text).ToList().Aggregate((x, y) => x + "; " + y);
        PixHeader.Replace("\n", String.Empty);
        PixHeader += "\n";
        LoggingSystem.CreateCSVHeader(LogFile.Pix, PixHeader);
    }

    internal void StartGame(int id)
    {
        Debug.Log("Starting Game.");
        Cursor.lockState = CursorLockMode.Locked;
        CreateLogFiles(id);
        LatinSquare.CalculateLatinSquareByID(ParticipantID);
        LatinSquare.PrintLatinSquare();
        _one = new CamCon(LatinSquare.PerspectiveOrder.Dequeue());
        _two = new CamCon(LatinSquare.PerspectiveOrder.Dequeue());
        _three = new CamCon(LatinSquare.PerspectiveOrder.Dequeue());
        _four = new CamCon(LatinSquare.PerspectiveOrder.Dequeue());

        _firstTask = LatinSquare.TaskOrder.Dequeue();
        _secondTask = LatinSquare.TaskOrder.Dequeue();
        _thirdTask = LatinSquare.TaskOrder.Dequeue();
        _forthTask = LatinSquare.TaskOrder.Dequeue();
        AkSoundEngine.StopPlayingID(BackgroundMusicMenu);
        BackgroundMusicGame = AkSoundEngine.PostEvent("MusicGame", gameObject);
        ChangeGameState(GameState.Level1);
    }

    public void ChangeGameState(GameState state)
    {       
        if (CurrentGameState.Equals(state))
        {
            Debug.LogWarning("Changing level to same state isn't supported.");
        }
        else
        {
            Debug.Log("Changing game state to: " + state.ToString());
            BlackFadeControl.Reciever.FadeOut(state);
        }
    }

    private void CompleteFadeIn(GameState nextState)
    {
        ChangingScene = false;
        FreeInput();
        switch (nextState)
        {
            case GameState.Start:
                BackgroundMusicMenu = AkSoundEngine.PostEvent("MusicMenu", gameObject);
                break;
            case GameState.Level1:
                LevelManager.StartLevel();
                LevelManager.StartLogging();
                //Cursor.lockState = CursorLockMode.Locked;
                break;
            case GameState.Level2:
                LevelManager.StartLevel();
                LevelManager.StartLogging();
                //Cursor.lockState = CursorLockMode.Locked;
                break;
            case GameState.Level3:
                LevelManager.StartLevel();
                LevelManager.StartLogging();
                //Cursor.lockState = CursorLockMode.Locked;
                break;
            case GameState.Level4:
                LevelManager.StartLevel();
                LevelManager.StartLogging();
                //Cursor.lockState = CursorLockMode.Locked;       
                break;
            case GameState.End:
                BackgroundMusicMenu = AkSoundEngine.PostEvent("MusicMenu", gameObject);
                break;
        }
    }

    private void CompleteFadeOut(GameState nextState)
    {
        CurrentGameState = nextState;
        switch (nextState)
        {
            case GameState.Start:              
                break;
            case GameState.Level1:
                SceneManager.LoadScene("Level_1");
                break;
            case GameState.Level2:
                SceneManager.LoadScene("Level_1"); 
                break;
            case GameState.Level3:
                SceneManager.LoadScene("Level_1");
                break;
            case GameState.Level4:
                SceneManager.LoadScene("Level_1");
                break;
            case GameState.End:
                AkSoundEngine.StopPlayingID(BackgroundMusicGame);
                SceneManager.LoadScene("MainMenu");
                break;
        }
    }

    private void SceneUnloaded(Scene s)
    {
        Debug.Log("Unloaded Scene: " + s.name);
    }

    private void SceneLoaded(Scene s, LoadSceneMode mode)
    {
        Debug.Log("Loaded Scene: " + s.name);

        switch (CurrentGameState)
        {
            case GameState.Start:
                break;
            case GameState.Level1:
                _questionaire.gameObject.GetComponent<QuestionaireController>().ParticipantID = ParticipantID;
                _questionaire.gameObject.GetComponent<QuestionaireController>().CondCamera = _one.Condtion;
                _questionaire.gameObject.GetComponent<QuestionaireController>().LNumber = (int) CurrentGameState;
                LevelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
                LevelManager.GameManager = this;
                LevelManager.GenerateLevel(new LevelParameter 
                {
                    CameraCondition = _one.Condtion,
                    FirstTask = _firstTask,
                    SecondTask = _secondTask,
                    ThirdTask = _thirdTask,
                    ForthTask = _forthTask,
                });
                if (_one.Condtion == ConditionPerpective.Free)
                {
                    LevelManager.FirstPopup = true;
                }

                break;
            case GameState.Level2:
                _questionaire.gameObject.GetComponent<QuestionaireController>().ParticipantID = ParticipantID;
                _questionaire.gameObject.GetComponent<QuestionaireController>().CondCamera = _two.Condtion;
                _questionaire.gameObject.GetComponent<QuestionaireController>().LNumber = (int)CurrentGameState;
                LevelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
                LevelManager.GameManager = this;
                LevelManager.GenerateLevel(new LevelParameter
                {
                    CameraCondition = _two.Condtion,
                    FirstTask = _firstTask,
                    SecondTask = _secondTask,
                    ThirdTask = _thirdTask,
                    ForthTask = _forthTask,
                });
                
                break;
            case GameState.Level3:
                _questionaire.gameObject.GetComponent<QuestionaireController>().ParticipantID = ParticipantID;
                _questionaire.gameObject.GetComponent<QuestionaireController>().CondCamera = _three.Condtion;
                _questionaire.gameObject.GetComponent<QuestionaireController>().LNumber = (int)CurrentGameState;
                LevelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
                LevelManager.GameManager = this;
                LevelManager.GenerateLevel(new LevelParameter
                {
                    CameraCondition = _three.Condtion,
                    FirstTask = _firstTask,
                    SecondTask = _secondTask,
                    ThirdTask = _thirdTask,
                    ForthTask = _forthTask,
                });
                
                break;
            case GameState.Level4:
                _questionaire.gameObject.GetComponent<QuestionaireController>().ParticipantID = ParticipantID;
                _questionaire.gameObject.GetComponent<QuestionaireController>().CondCamera = _four.Condtion;
                _questionaire.gameObject.GetComponent<QuestionaireController>().LNumber = (int)CurrentGameState;
                LevelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
                LevelManager.GameManager = this;
                LevelManager.GenerateLevel(new LevelParameter
                {
                    CameraCondition = _four.Condtion,
                    FirstTask = _firstTask,
                    SecondTask = _secondTask,
                    ThirdTask = _thirdTask,
                    ForthTask = _forthTask,
                });
                        
                break;
            case GameState.End:
                Menu = GameObject.Find("StartMenu").GetComponent<StartMenuObservator>();
                Cursor.lockState = CursorLockMode.None;
                break;
        }
        BlackFadeControl.Reciever.FadeIn(CurrentGameState);
    }

    internal class CamCon
    {
        internal ConditionPerpective Condtion;
        internal CameraPerspective Perspective;
        internal bool Locked;

        internal CamCon(ConditionPerpective con)
        {
            Condtion = con;
            switch (con)
            {
                case ConditionPerpective.First:
                    Perspective = CameraPerspective.First;
                    Locked = true;
                    break;
                case ConditionPerpective.Third:
                    Perspective = CameraPerspective.Third;
                    Locked = true;
                    break;
                case ConditionPerpective.Upper:
                    Perspective = CameraPerspective.UpperThird;
                    Locked = true;
                    break;
                case ConditionPerpective.Free:
                    Perspective = CameraPerspective.Third;
                    Locked = false;
                    break;
            }
        }
    }

    public ConditionPerpective GetCurrentCameraCondition()
    {
        switch (CurrentGameState)
        {
            case GameState.Start:
                break;
            case GameState.Level1:
                return _one.Condtion;
            case GameState.Level2:
                return _two.Condtion;
            case GameState.Level3:
                return _three.Condtion;
            case GameState.Level4:
                return _four.Condtion;
            case GameState.End:
                break;

        }
        return ConditionPerpective.Third;
    }

    public int GetTaskNumber(TaskType t)
    {
        if (t == _firstTask) return 1;
        else if (t == _secondTask) return 2;
        else if (t == _thirdTask) return 3;
        else if (t == _forthTask) return 4;
        else return -1;
    }
}


public enum GameState
{
    Start = -1,
    Level1 = 1,
    Level2 = 2,
    Level3 = 3,
    Level4 = 4,
    End = -2,
}
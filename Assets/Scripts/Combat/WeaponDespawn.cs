using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponDespawn : MonoBehaviour
{
    public GameObject Player;
    public Material disMaterial;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectsWithTag("Player")[0];
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Player.GetComponentInChildren<WeaponController>().GetComponentInChildren<MeshRenderer>().material.SetFloat("_TimerCount", -1);
            Player.GetComponentInChildren<WeaponController>().GetComponentInChildren<MeshRenderer>().material = disMaterial;
            StartCoroutine(RemoveCD());
        }
    }
    IEnumerator RemoveCD()
    {
        float timer = -1;
        while(timer <= 1)
        {
            yield return new WaitForSeconds(0.05f);
            timer += 0.05f;
            Player.GetComponentInChildren<WeaponController>().GetComponentInChildren<MeshRenderer>().material.SetFloat("_TimerCount", timer);

        }

        Player.GetComponentInChildren<WeaponController>().removeWeapon();
    }
}



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCombat : MonoBehaviour
{
    [SerializeField]
    private float attackspeed = 1f;
    private float attackCooldown = 0f;

    // Attack Function
   public void Attack()
    {
        if (attackCooldown <= 0f)
        {

            attackCooldown = 1f / attackspeed;
        }
    }

    void Update() => attackCooldown -= Time.deltaTime;
}

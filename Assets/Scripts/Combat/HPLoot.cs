using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPLoot : MonoBehaviour
{
    private int healingValue = 20;  // Value of the HPLoot item
    private GameObject player; // Player Object
    private float pickUpRange = 4f; // How close the player needs to be to pick it up

    private void Start()
    {
        player = player = GameObject.Find("Player(Clone)").transform.GetChild(4).gameObject;

    }


    // Update is called once per frame
    void Update()
    {
        // TODO: Hover animation idk?

        // Player walks over object
        OnPlayerWalkOver();
    }

    // Player walks over Object to pick it up
    private void OnPlayerWalkOver()
    {
        // Distance between player and hp object. Pick it up if its smaller then the defined pickup range
        if(Vector3.Distance(player.transform.position, transform.position) < pickUpRange)
        {
            // Get the Gamehandler from the Player
            GameHandler playerHandler = player.GetComponent<GameHandler>();
            
            // Check if Player even needs hp pickup
            if(playerHandler.healthSystem.GetHealthPercent() < 1f)
            {
                // Heal Player and destroy the hp item
                playerHandler.healthSystem.Heal(healingValue);
                Destroy(gameObject);
            }
        }
    }
}

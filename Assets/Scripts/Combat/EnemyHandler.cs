using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHandler : MonoBehaviour
{
    public HealthSystem healthSystem;
    public GameHandler gameHandler;
    public GameObject loot;
    public int health = 100;

    // Start is called before the first frame update
    void Start()
    {
        // Health for the enemys get initialized
        // TODO: Change max health for different enemys?
        healthSystem = new HealthSystem(health);
        var player = GameObject.Find("Player(Clone)");
        gameHandler = player.GetComponentInChildren<GameHandler>();
    }

    // Update is called once per frame
    void Update()
    {
        // IF enemy HP is 0 destroy the game Object
        if(healthSystem.GetHealth() == 0)
        {
            // Drops a predefined Loot Item
            // Can add RNG if its too easy
            Destroy(this.gameObject);
        }
    }
}

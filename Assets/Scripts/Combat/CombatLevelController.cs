using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatLevelController : MonoBehaviour
{
    public GameObject EntryDoor;
    public GameObject ExitDoor;
    public List<GameObject> Enemies;

    // Start is called before the first frame update
    void Start()
    {
            
    }

    // Update is called once per frame
    void Update()
    {
        foreach(GameObject enemie in Enemies)
        {
            if(enemie == null)
            {
                Enemies.Remove(enemie);
                break;
            }
        }

        if(Enemies.Count == 0)
        {
            OpenDoor();
        }
    }

    void OpenDoor()
    {
        ExitDoor.SetActive(false);
    }

    public void CloseDoor()
    {
        EntryDoor.SetActive(true);
    }
}

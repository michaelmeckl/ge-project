using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject Sword;
    public GameObject player;
    public bool CanAttack = true;
    public float AttackCooldown = 0f;
    public bool isAttacking;
    public bool hasWeapon;
    public List<GameObject> hasAttacked;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (CanAttack)
            {
                SwordAttack();
            }
        }    
    }

    public void giveWeapon()
    {
        hasWeapon = true;
        CanAttack = true;
        SpawnSword();
    }

    public void removeWeapon()
    {
        hasWeapon = false;
        CanAttack = false;
        DeSpawnSword();
    }

    public void SwordAttack()
    { 
            isAttacking = true;
            CanAttack = false;
            //Animator anim = Sword.GetComponent<Animator>();
           // anim.SetTrigger("Attack");
            StartCoroutine(ResetAttackCooldown());
    }


    IEnumerator ResetAttackCooldown()
    {
        StartCoroutine(ResetAttackBoolean());
        yield return new WaitForSeconds(AttackCooldown);
        CanAttack = true;
    }

    IEnumerator ResetAttackBoolean()
    {
        yield return new WaitForSeconds(1f);
        isAttacking = false;
        hasAttacked = new List<GameObject>();
    }

    private void SpawnSword()
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
    }

    private void DeSpawnSword()
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour
{
    public WeaponController wc;
    public GameObject HitParticle;

    // Checks if enemy got hit by player
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy" && wc.isAttacking && !wc.hasAttacked.Contains(gameObject))
        {
            AkSoundEngine.PostEvent("SwordHit", gameObject);
            // Animation of Enemy
            Debug.Log(other.name + " hit!");
            other.GetComponent<EnemyHandler>().healthSystem.Damage(20);

            //Knockback of Enemy
            Vector3 moveDirection = other.transform.position - transform.position ;
            other.GetComponent<Rigidbody>().AddForce(moveDirection.normalized * 1000f);
            if (other)
            {
                StartCoroutine(ResetForceCoroutine(other, moveDirection));
            }
            // Particle spawn if needed
            HitParticle.GetComponent<ParticleSystem>().Play();

            wc.hasAttacked.Add(gameObject);
        }
    }
    IEnumerator ResetForceCoroutine(Collider enemy, Vector3 moveDirection)
    {
        enemy.gameObject.GetComponent<StealthAi>().notPushed = false;
        yield return new WaitForSeconds(1);
        if (enemy)
        {
            enemy.GetComponent<Rigidbody>().velocity = Vector3.zero;
            enemy.gameObject.GetComponent<StealthAi>().notPushed = true;
        }
    }
}

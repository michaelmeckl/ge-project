using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSpawn : MonoBehaviour
{
    public GameObject Player;
    public CombatLevelController controller;

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player(Clone)");
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            Player.GetComponentInChildren<WeaponController>().giveWeapon();
            controller.CloseDoor();
        }

    }
}

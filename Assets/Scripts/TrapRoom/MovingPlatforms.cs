using System;
using UnityEngine;

public class MovingPlatforms : MonoBehaviour
{
    [SerializeField]
    private float speed = 5f;

    private Transform _movingPlatformChild;

    private void Awake()
    {
        _movingPlatformChild = transform.GetChild(1);
    }

    private void FixedUpdate()
    {
        if (GameController.isGamePaused) return;   // don't move platform while the game is paused
        
        var movement = Vector3.forward * Time.deltaTime * speed;
        _movingPlatformChild.Translate(movement);
    }

    // Upon collision with another GameObject, this GameObject will reverse direction.
    // As long as the parent object has a Rigidbody this trigger also bubbles up from child collisions.
    private void OnTriggerEnter(Collider other)
    {
        // make sure the trigger collision was the custom start and end triggers of the rail
        if (other.CompareTag("MovingPlatformBound"))
        {
            speed = speed * -1;
        }
    }
}

// TODO alternative: move platform along waypoints
// taken from https://medium.com/nerd-for-tech/moving-platforms-in-unity-86d0c6f05d85

/*
[SerializeField] private List<Transform> waypoints;
[SerializeField] private float moveSpeed = 5f;
private int _currentWaypoint;

private void Start()
{
    if (waypoints.Count <= 0) return;
    _currentWaypoint = 0;
}

private void FixedUpdate()
{
    HandleMovement();
}

private void HandleMovement()
{
  
    transform.position = Vector3.MoveTowards(transform.position, waypoints[_currentWaypoint].transform.position,
        (moveSpeed * Time.deltaTime));

    if (Vector3.Distance(waypoints[_currentWaypoint].transform.position, transform.position) <= 0)
    {
        _currentWaypoint++;
    }

    if (_currentWaypoint != waypoints.Count) return;
    waypoints.Reverse();
    _currentWaypoint = 0;
}
*/
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// NOT USED
public class RandomiseTrapRoom : MonoBehaviour
{
    /// <summary>
    /// The different layout versions of the acid pool crossing.
    /// </summary>
    [SerializeField]
    private List<GameObject> trapRoomLayouts;

    // TODO set the start position relative to the trap room prefab overall
    // The position where to instantiate the acid pool crossing.
    // private Transform layoutStartPosition;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;
        print("Starting to randomise trap room layout ...");

        var chosenLayout = GetRandomAcidPoolCrossing();
        print("chosenLayout: " + chosenLayout.name);
        // TODO instantiate it in the correct position
        // Instantiate(chosenLayout, startPosition, startRotation);
        
        print("Finished generating random trap room layout!");
    }

    private GameObject GetRandomAcidPoolCrossing()
    {
        return trapRoomLayouts[Random.Range(0, trapRoomLayouts.Count)];
    }
}
using System;
using System.Collections;
using UnityEngine;

public class EnterBrokenWoodPlank : MonoBehaviour
{
    [SerializeField]
    private GameObject damagedWoodPlank;

    private GameHandler _gameHandler;
    
    // add a short time before the plank breaks
    private float waitTime = 0.5f;

    private void Start()
    {
        var player = GameObject.Find("Player(Clone)");
        _gameHandler = player.GetComponentInChildren<GameHandler>();
        _gameHandler.playerRespawnedEvent += RespawnPlank;
    }

    private void RespawnPlank()
    {
        // if check necessary to prevent crash!
        if (damagedWoodPlank)
        {
            damagedWoodPlank.SetActive(true);
        }
    }


    private IEnumerator OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            yield return new WaitForSeconds(waitTime);
            damagedWoodPlank.SetActive(false);
        }
    }
}
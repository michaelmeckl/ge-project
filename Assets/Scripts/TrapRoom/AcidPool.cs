using UnityEngine;

public class AcidPool : MonoBehaviour
{
    public GameObject player;
    private void Start()
    {
        player = GameObject.Find("Player(Clone)");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            print("Player died a horrible death because he entered the Acid Pool :(");
            player.GetComponentInChildren<GameHandler>().healthSystem.Damage(100);
        }
    }
}
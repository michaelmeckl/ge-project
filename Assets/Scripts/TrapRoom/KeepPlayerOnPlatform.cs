using UnityEngine;

public class KeepPlayerOnPlatform : MonoBehaviour
{
    Transform tempParent;
    private void OnTriggerEnter(Collider other)
    {
        // parent the player to the moving platform's transform so the player will stay at the top of the platform
        if (other.CompareTag("Player"))
        {
            // print("Player entered platform");
            tempParent = other.transform.parent;
            other.transform.parent = transform;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.parent = tempParent;
        }
    }
    
    /*
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            print("Player entered platform");
            collision.transform.parent = transform;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.transform.parent = null;
        }
    }
    */
}
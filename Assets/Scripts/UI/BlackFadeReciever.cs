using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackFadeReciever : MonoBehaviour
{
    public delegate void OnFadeFinish(GameState nextState);

    public OnFadeFinish FadeInCallback;
    public OnFadeFinish FadeOutCallback;

    public Animator Anim;

    private GameState _nextState;

    private void Awake()
    {
        _nextState = GameState.Start;
    }

    public void FadeIn(GameState nextState)
    {
        _nextState = nextState;
        Anim.SetTrigger("FadeInTrigger");
    }

    public void FadeOut(GameState nextState)
    {
        _nextState = nextState;
        Anim.SetTrigger("FadeOutTrigger");
    }

    public void FadeInComplete()
    {
        FadeInCallback?.Invoke(_nextState);
    }

    public void FadeOutComplete()
    {
        FadeOutCallback?.Invoke(_nextState);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackButton : MonoBehaviour
{
    public delegate void OnButtonClick();

    public OnButtonClick Callback;

    private void OnMouseUpAsButton()
    {
        Callback?.Invoke();
    }
}

using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private HealthSystem healthSystem;
    private Transform healthbar;

    public void Setup(HealthSystem healthSystem)
    {
        this.healthSystem = healthSystem;
        healthbar = transform.Find("Bar");
    }

    private void Update()
    {
        healthbar.localScale = new Vector3(healthSystem.GetHealthPercent(), 1);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class StartMenuObservator : MonoBehaviour
{
    public TMPro.TMP_InputField Input;

    public Slider VolumeSlider;

    public Button StartButton;
    public Button ExitButton;

    public delegate void OnButtonClick(int participantID);

    public OnButtonClick StartButtonCallback;
    public OnButtonClick ExitButtonCallback;

    // Start is called before the first frame update
    void Start()
    {
        StartButton.onClick.AddListener(ClickStartButton);
        ExitButton.onClick.AddListener(ClickExitButton);
        VolumeSlider.onValueChanged.AddListener(VolumeSliderChange);
        Input.onValueChanged.AddListener((string s) => AkSoundEngine.PostEvent("Key", gameObject));
        VolumeSlider.value = GameController.MasterVolume;
    }

    // Update is called once per frame
    void Update()
    {

        if(!ValidInput())
        {
            StartButton.interactable = false;
        }
        else
        {
            StartButton.interactable = true;
        }
    }

    public int GetParticipantID()
    {
        if(ValidInput())
        {
            return int.Parse(Input.text);
        }
        else
        {
            return 99999;
        }
    }

    private bool ValidInput()
    {
        return !(Input.text.Equals(string.Empty) || Input.text.Equals(""));
    }

    void VolumeSliderChange(float value)
    {
        GameController.MasterVolume = value;
        AkSoundEngine.SetRTPCValue("MasterVolume", value*100);
    }

    void ClickStartButton()
    {
        AkSoundEngine.PostEvent("ButtonClickStart", gameObject);
        StartButtonCallback?.Invoke(int.Parse(Input.text));
    }

    void ClickExitButton()
    {
        AkSoundEngine.PostEvent("ButtonClickLeave", gameObject);
        Application.Quit();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseController : MonoBehaviour
{
    public Button Continue;
    public Button Exit;

    public Slider Volume;

    public delegate void OnButtonClick();
    public OnButtonClick StartButtonCallback;
    public OnButtonClick ExitButtonCallback;

    public CursorLockMode PreviousCursorState;

    // Start is called before the first frame update
    void Start()
    {
        Continue.onClick.AddListener(ClickStartButton);
        Exit.onClick.AddListener(ClickExitButton);
        Volume.onValueChanged.AddListener(VolumeSliderChange);
        Volume.value = GameController.MasterVolume;
    }

    void VolumeSliderChange(float value)
    {
        GameController.MasterVolume = value;
        AkSoundEngine.SetRTPCValue("MasterVolume", value * 100);
    }

    void ClickStartButton()
    {
        AkSoundEngine.PostEvent("ButtonClick", gameObject);
        StartButtonCallback?.Invoke();
    }

    void ClickExitButton()
    {
        AkSoundEngine.PostEvent("ButtonClickLeave", gameObject);
        ExitButtonCallback?.Invoke();
        Application.Quit();
    }
}

using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{

    public Vector3 PositonWall;
    public Vector3 PositionFloor;

    public bool Floor = false;

    public bool HideCameraChange = false;

    [SerializeField]
    private TMPro.TMP_Text _cameraChangeLabel;
    [SerializeField]
    private RawImage _cameraChangeIcon1;
    [SerializeField]
    private RawImage _cameraChangeIcon2;
    [SerializeField]
    private RawImage _cameraChangeIcon3;

    public bool HideLean = false;

    [SerializeField]
    private TMPro.TMP_Text _leanLabel;
    [SerializeField]
    private RawImage _leanIcon1;
    [SerializeField]
    private RawImage _leanIcon2;


    // Update is called once per frame
    void Update()
    {
        if(Floor)
        {
            transform.position = PositionFloor;
            transform.rotation = Quaternion.Euler(90, 0, 0);
        }
        else
        {
            transform.position = PositonWall;
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        if(HideCameraChange)
        {
            _cameraChangeLabel.enabled = false;
            _cameraChangeIcon1.enabled = false;
            _cameraChangeIcon2.enabled = false;
            _cameraChangeIcon3.enabled = false;
        }
        else
        {
            _cameraChangeLabel.enabled = true;
            _cameraChangeIcon1.enabled = true;
            _cameraChangeIcon2.enabled = true;
            _cameraChangeIcon3.enabled = true;

        }

        if (HideLean)
        {
            _leanLabel.enabled = false;
            _leanIcon1.enabled = false;
            _leanIcon2.enabled = false;

        }
        else
        {
            _leanLabel.enabled = true;
            _leanIcon1.enabled = true;
            _leanIcon2.enabled = true;
        }
    }


}

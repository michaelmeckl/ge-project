using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LikertData : MonoBehaviour
{
    public bool Reversed;

    public Toggle Toggle0;
    public Toggle Toggle1;
    public Toggle Toggle2;
    public Toggle Toggle3;
    public Toggle Toggle4;
    public Toggle Toggle5;
    public Toggle Toggle6;

    public bool Valid { get => CheckMarkValues.Any(x => x.Value == true); }

    public Dictionary<int, bool> CheckMarkValues
    {
        get
        { 
            if(Reversed)
            {
                return new Dictionary<int, bool>
                {
                    {6, Toggle0.isOn },
                    {5, Toggle1.isOn },
                    {4, Toggle2.isOn },
                    {3, Toggle3.isOn },
                    {2, Toggle4.isOn },
                    {1, Toggle5.isOn },
                    {0, Toggle6.isOn },
                };
            }
            else
            {
                return new Dictionary<int, bool>
                {
                    {0, Toggle0.isOn },
                    {1, Toggle1.isOn },
                    {2, Toggle2.isOn },
                    {3, Toggle3.isOn },
                    {4, Toggle4.isOn },
                    {5, Toggle5.isOn },
                    {6, Toggle6.isOn },
                };
            }
        }
            
           
    }

    public int Value { get => CheckMarkValues.FirstOrDefault(x => x.Value == true).Key+1; }

    private void Start()
    {
        Toggle0.onValueChanged.AddListener(Click);
        Toggle1.onValueChanged.AddListener(Click);
        Toggle2.onValueChanged.AddListener(Click);
        Toggle3.onValueChanged.AddListener(Click);
        Toggle4.onValueChanged.AddListener(Click);
        Toggle5.onValueChanged.AddListener(Click);
        Toggle6.onValueChanged.AddListener(Click);
    }

    private void Click(bool v)
    {
        AkSoundEngine.PostEvent("ButtonClick", gameObject);
        //Debug.Log(Value);

    }

    public void Reset()
    {
        Toggle0.isOn = false;
        Toggle1.isOn = false;
        Toggle2.isOn = false;
        Toggle3.isOn = false;
        Toggle4.isOn = false;
        Toggle5.isOn = false;
        Toggle6.isOn = false;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemData : MonoBehaviour
{
    public TMPro.TMP_Text Label;
    [HideInInspector]
    public string Question { get => Label?.text; }
    public LikertData Data;
    public Subscale Subscale;
    public Scale Scale;

}

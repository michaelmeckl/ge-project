using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Page : MonoBehaviour
{
    public Scale Scale;
    public List<Subscale> SubscalesOnPage;

    public float TopOffset = -75;

    public GameObject[] Items;

    public int PageItemLimit = 7;

    public bool Valid
    {
        get => Items.All(x => x.GetComponent<ItemData>().Data.Valid);
    }

    private void Update()
    {

        for(int i = 0; i < Items.Length; i++)
        {           
            if (i >= PageItemLimit)
            {
                Items[i].SetActive(false);
                continue;
            }
            else
            {
                Items[i].SetActive(true);
            }

            var posy = (i * -150) + TopOffset;

            Items[i].GetComponent<RectTransform>().anchoredPosition = new Vector3(Items[i].GetComponent<RectTransform>().anchoredPosition.x, posy);
        }
    }
}

public enum Scale
{
    None,
    Imi,
    Pix
}

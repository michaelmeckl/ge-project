using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Questionaire : MonoBehaviour
{
    public ScaleEntry[] ImiInterestData;
    public ScaleEntry[] ImiCompetenceData;

    public ScaleEntry[] PixAutonomyData;
    public ScaleEntry[] PixImmersionData;

    public List<ScaleEntry> Data
    {
        get
        {
            var result = new List<ScaleEntry>();
            result.AddRange(ImiInterestData);
            result.AddRange(ImiCompetenceData);
            result.AddRange(PixAutonomyData);
            result.AddRange(PixImmersionData);
            return result;
        }
    }
}

[Serializable]
public class ScaleEntry
{
    public Subscale Scale;
    public ItemData ItemData;

    public void PrintScaleEntry()
    {
        Debug.Log(ItemData.Question + ": " + ItemData.Data.Value);
    }
}

public enum Subscale
{
    None,
    Interest,
    Competence,
    Choice,
    Mastery,
    Autonomy,
    Immersion,
    Ease
}

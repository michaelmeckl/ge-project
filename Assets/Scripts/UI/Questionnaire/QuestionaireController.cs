using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;
using System;

public class QuestionaireController : MonoBehaviour
{
    private Questionaire _questionaire;
    private int _currentPosition;
    private Page _currentPage;

    public delegate void OnQuestionaireFinish();
    public OnQuestionaireFinish Callback;

    public Camera Camera;

    public Page[] QuestionaireOrder;

    public Page Begin;
    public Page End;

    public Button Back;
    public Button Next;

    public Button Save;

    internal int ParticipantID;
    internal ConditionPerpective CondCamera;
    internal int LNumber;

    private bool _invalid;

    //private float takt = 2f;
    //private float dtrest;

    // Start is called before the first frame update
    void Start()
    {
        _questionaire = GetComponent<Questionaire>();
        _currentPosition = -1;
        _currentPage = Begin;
        Back.onClick.AddListener(ClickBackButton);
        Next.onClick.AddListener(ClickNextButton);
        Save.onClick.AddListener(ClickSaveButton);
        _invalid = false;
        //dtrest = takt;
    }

    // Update is called once per frame
    void Update()
    {
        /*dtrest -= Time.deltaTime;
        if(dtrest < 0)
        {
            foreach (var e in _questionaire.Data) e.PrintScaleEntry();
            dtrest = takt;
        }*/

        if(_currentPage.Equals(Begin))
        {
            Back.interactable = false;
            Next.interactable = true;
            Back.gameObject.SetActive(false);
            Next.gameObject.SetActive(true);
            Begin.gameObject.SetActive(true);
            End.gameObject.SetActive(false);
            foreach (var e in QuestionaireOrder)
            {
                e.gameObject.SetActive(false);
            }
        }
        else if(_currentPage.Equals(End))
        {
            Back.gameObject.SetActive(true);
            Next.gameObject.SetActive(false);
            Begin.gameObject.SetActive(false);
            End.gameObject.SetActive(true);
            Back.interactable = true;
            Next.interactable = false;
            foreach (var e in QuestionaireOrder)
            {
                e.gameObject.SetActive(false);
            }
        }
        else
        {
            Back.gameObject.SetActive(true);
            Next.gameObject.SetActive(true);
            Begin.gameObject.SetActive(false);
            End.gameObject.SetActive(false);
            Back.interactable = true;
            _currentPage = QuestionaireOrder[_currentPosition];
            _currentPage.gameObject.SetActive(true);
            foreach(var e in QuestionaireOrder)
            {
                if (e.Equals(_currentPage)) continue;

                e.gameObject.SetActive(false);
            }
        }

        if (!_currentPage.Valid)
        {
            Next.interactable = false;
        }
        else
        {
            Next.interactable = true;
        }
    }

    public void ShuffleScales()
    {
        var Imi = QuestionaireOrder.Take(2).ToArray();
        Util.Shuffle(Imi);
        QuestionaireOrder[0] = Imi[0];
        QuestionaireOrder[1] = Imi[1];

        foreach (var e in QuestionaireOrder)
        {
            Util.Shuffle(e.Items);
        }

    }

    private void ClickBackButton()
    {
        AkSoundEngine.PostEvent("ButtonClick", gameObject);
        _currentPosition--;
        if (_currentPosition >= 0)
        {
            _currentPage = QuestionaireOrder[_currentPosition];        
        }
        else
        {
            _currentPage = Begin;
        }
    }

    private void ClickNextButton()
    {
        AkSoundEngine.PostEvent("ButtonClick", gameObject);
        _currentPosition++;
        if (_currentPosition <= QuestionaireOrder.Length-1)
        {
            _currentPage = QuestionaireOrder[_currentPosition];
        }
        else
        {
            _currentPage = End;
        }
    }

    public void Reset()
    {
        foreach(var e in _questionaire.Data)
        {
            e.ItemData.Data.Reset();
        }
        _currentPosition = -1;
        _currentPage = Begin;
        _invalid = false;
    }

    private void ClickSaveButton()
    {
        AkSoundEngine.PostEvent("ButtonClickStart", gameObject);
        //ToDo
        LoggingSystem.AppendImiSet(new IMIDataSet
        {
            TimeStamp = DateTime.Now,
            TimeStampUnixTime = ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds(),

            ID = ParticipantID,
            ConditionCamera = CondCamera,
            LevelNumber = LNumber,

            CompetenceItem1 = _questionaire.ImiCompetenceData[0].ItemData.Data.Value,
            CompetenceItem2 = _questionaire.ImiCompetenceData[1].ItemData.Data.Value,
            CompetenceItem3 = _questionaire.ImiCompetenceData[2].ItemData.Data.Value,
            CompetenceItem4 = _questionaire.ImiCompetenceData[3].ItemData.Data.Value,
            CompetenceItem5 = _questionaire.ImiCompetenceData[4].ItemData.Data.Value,
            CompetenceItem6 = _questionaire.ImiCompetenceData[5].ItemData.Data.Value,

            EnjoymentItem1 = _questionaire.ImiInterestData[0].ItemData.Data.Value,
            EnjoymentItem2 = _questionaire.ImiInterestData[1].ItemData.Data.Value,
            EnjoymentItem3 = _questionaire.ImiInterestData[2].ItemData.Data.Value,
            EnjoymentItem4 = _questionaire.ImiInterestData[3].ItemData.Data.Value,
            EnjoymentItem5 = _questionaire.ImiInterestData[4].ItemData.Data.Value,
            EnjoymentItem6 = _questionaire.ImiInterestData[5].ItemData.Data.Value,
            EnjoymentItem7 = _questionaire.ImiInterestData[6].ItemData.Data.Value,
        }); 
        LoggingSystem.AppendPixSet(new PIXDataSet
        {
            TimeStamp = DateTime.Now,
            TimeStampUnixTime = ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds(),

            ID = ParticipantID,
            ConditionCamera = CondCamera,
            LevelNumber = LNumber,

            AutonomyItem1 = _questionaire.PixAutonomyData[0].ItemData.Data.Value,
            AutonomyItem2 = _questionaire.PixAutonomyData[1].ItemData.Data.Value,
            AutonomyItem3 = _questionaire.PixAutonomyData[2].ItemData.Data.Value,

            ImmersionItem1 = _questionaire.PixImmersionData[0].ItemData.Data.Value,
            ImmersionItem2 = _questionaire.PixImmersionData[1].ItemData.Data.Value,
            ImmersionItem3 = _questionaire.PixImmersionData[2].ItemData.Data.Value,
        });
        Callback?.Invoke();
    }
}

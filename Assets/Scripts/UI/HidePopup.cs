using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidePopup : MonoBehaviour
{
    public delegate void OnbuttonClick();
    public OnbuttonClick Callback;

    public void hidePopup()
    {
        gameObject.SetActive(false);
        Callback?.Invoke();
    }
}

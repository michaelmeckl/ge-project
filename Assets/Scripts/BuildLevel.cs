using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildLevel : MonoBehaviour
{
    public GameObject StartRoom;
    public GameObject EndRoom;

    public GameObject PuzzleRoom;
    public float Room1OffsetSizeX;
    public GameObject CombatRoom;
    public float Room2OffsetSizeX;
    public GameObject DexRoom;
    public float Room3OffsetSizeX;
    public GameObject StealthRoom;
    public float Room4OffsetSizeX;
    public TaskType[] order;

    private float roomOffsetSizeX;
    private float lastRoomOffset;
    private float lastroomSizeY;
    private float roomSizeY;
    public float specialZ = 0;
    private float specialZOffset = 0;
    public float specialOffset = 0;
    private float lastSpecialOffset = 0;
    // Start is called before the first frame update


    public void StartRoomGenereation()
    {
        GenerateRooms();
    }

    void GenerateRooms()
    {
        StartRoom = Instantiate(StartRoom, Vector3.zero, Quaternion.identity, transform);
        GameObject last = new GameObject();
        foreach(var i in order)
        {

            if(i == order[0])
            {
                last = Instantiate(getRoomFromNumber(i), new Vector3(0,0,0), Quaternion.identity, transform);

            }
            else
            {
                last = Instantiate(getRoomFromNumber(i), new Vector3(last.transform.position.x + lastRoomOffset, 0 + lastSpecialOffset, last.transform.position.z + lastroomSizeY + specialZOffset - 0.6f), Quaternion.identity, transform);
                
            }
            RoomAllocation(last, i);
            lastSpecialOffset = specialOffset;
            specialZOffset = specialZ;
        }
        EndRoom = Instantiate(EndRoom, new(last.transform.position.x + roomOffsetSizeX, 0 + specialOffset, last.transform.position.z + roomSizeY - 0.6f ), Quaternion.identity, transform);
    }

    GameObject getRoomFromNumber(TaskType type)
    {

        GameObject room = PuzzleRoom;
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

        switch (type)
        {
            case TaskType.Puzzle:
                room = PuzzleRoom;
                bounds = CalculateBounds(room);

                lastroomSizeY = roomSizeY;
                roomSizeY = bounds.size.z;

                lastRoomOffset = roomOffsetSizeX;

                roomOffsetSizeX = Room1OffsetSizeX;
                break;
            case TaskType.Combat:
                room = CombatRoom;
                bounds = CalculateBounds(room);
                lastroomSizeY = roomSizeY;
                Debug.Log(bounds.size);

                //specialZ = -13.5f;
                lastRoomOffset = roomOffsetSizeX;
                roomSizeY = bounds.size.z;
                roomOffsetSizeX = Room2OffsetSizeX;
                break;
            case TaskType.Dexterity:
                room = DexRoom;
                bounds = CalculateBounds(room);
                lastRoomOffset = roomOffsetSizeX;

                lastroomSizeY = roomSizeY;
                roomSizeY = bounds.size.z;
                specialOffset = 12.7f;

                roomOffsetSizeX = Room3OffsetSizeX;
                break;
            case TaskType.Stealth:
                room = StealthRoom;
                bounds = CalculateBounds(room);
                lastroomSizeY = roomSizeY;

                roomSizeY = bounds.size.z;
                lastRoomOffset = roomOffsetSizeX;
                roomOffsetSizeX = Room4OffsetSizeX;
                break;
        }

        return room;
    }

    private void RoomAllocation(GameObject instRoom, TaskType number)
    {
        switch (number)
        {
            case TaskType.Puzzle:
                PuzzleRoom = instRoom;
                break;
            case TaskType.Combat:
                CombatRoom = instRoom;
                break;
            case TaskType.Dexterity:
                DexRoom = instRoom;
                break;
            case TaskType.Stealth:
                StealthRoom = instRoom;
                break;
        }
    }

    private Bounds CalculateBounds(GameObject gameObject)
    {
        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
        GameObject instance = GameObject.Instantiate(gameObject, Vector3.zero, Quaternion.identity) as GameObject;

        Collider[] colliders = instance.GetComponentsInChildren<Collider>();
        foreach (Collider col in colliders)
        {
            bounds.Encapsulate(col.bounds);
        }

        GameObject.Destroy(instance);

        return bounds;
    }


}

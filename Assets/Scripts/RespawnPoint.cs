using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RespawnPoint : MonoBehaviour
{

    public GameObject spawnPosition;
    public GameObject player;
    public string TaskText;

    private bool interacted = false;

    private void Start()
    {
        player = GameObject.Find("Player(Clone)");
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            // Only be able to interact with the respawn point once
            if (!interacted)
            {
                var text = player.transform.Find("Canvas").transform.Find("LowerText").GetComponent<TextMeshProUGUI>();
                var background = player.transform.Find("Canvas").transform.Find("InteractableInformation");
                text.text = TaskText;
                text.gameObject.SetActive(true);
                background.gameObject.SetActive(true);
                StartCoroutine(hideText(text.gameObject, background.gameObject));
                // Sets the respawn Point
                SetRespawnPoint();
            }
        }
    }


    IEnumerator hideText(GameObject text, GameObject background)
    {
        yield return new WaitForSeconds(3);
        text.SetActive(false);
        background.SetActive(false);
    }

    // Sets the new SpawnPoint to the spawnPosition
    private void SetRespawnPoint()
    {
        // interacted to true so the player can only set his spawnpoint once to each spawn location
        interacted = true;

        // Gets the GameHandler from the player and sets the respawn point to this spawn position
        player.GetComponentInChildren<GameHandler>().respawnPoint = spawnPosition;
        player.GetComponentInChildren<GameHandler>().healthSystem.Heal(100);


    }


}

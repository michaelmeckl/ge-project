using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    public HealthBar healthbar;
    public GameObject DeathScreen;
    public GameObject player;
    public GameObject informationText;
    public GameObject respawnPoint;
    public GameController controller;
    public string EntryText;

    public HealthSystem healthSystem;

    internal int Deaths;

    private bool _alive;
    
    internal delegate void OnPlayerRespawned();
    internal OnPlayerRespawned playerRespawnedEvent;

    
    // Start is called before the first frame update
    void Start()
    {
        // Set the player HP and set up the HP to display the HP
        _alive = true;
        healthSystem = new HealthSystem(100);
        healthbar.Setup(healthSystem);
        controller = GameObject.Find("GameManager").GetComponent<GameController>();
        respawnPoint = GameObject.Find("RespawnPoint");
        // Give the player his First "Quest" for debug purposes
        // TODO: Function to update and change the quest from another class?
        TextMeshProUGUI informationTextObject = informationText.GetComponent<TextMeshProUGUI>();
        informationTextObject.text = EntryText;
    }

    // Update is called once per frame
    void Update()
    {
        // Checks if the player is alive 
        if(healthSystem.GetHealth() == 0 && _alive)
        {
            _alive = false;
            Deaths++;
            // Set Cursor as visable so the player can interact with the Death Screen
            Cursor.lockState = CursorLockMode.None;
            // Set the DeathScreen as active
            DeathScreen.SetActive(true);

        }

        if (healthSystem.GetHealth() == 0)
        {

            controller.LockInput();
            controller.PauseGameSpeed();
        }
    }

    // Function to update the Text of the "Quest"
    public void updateText(string text)
    {
        // Sets the text to the text input
        TextMeshProUGUI informationTextObject = informationText.GetComponent<TextMeshProUGUI>();
        informationTextObject.text = text;
    }

    // Respawns the player
    public void Respawn()
    {
        _alive = true;
        // Sets the health to 100 again
        healthSystem.Heal(100);
        // Sets the deathscreen as inactive again
        DeathScreen.SetActive(false);
        // Set the Player as alive and changes his position to the Respawn Point
        Debug.Log(transform.position);
        Physics.SyncTransforms();
        transform.position = respawnPoint.transform.position;

        Debug.Log(transform.position);
        Cursor.lockState = CursorLockMode.Locked;
        controller.FreeInput();
        controller.ResumeGameSpeed();

        // notify observers about the player respawning
        playerRespawnedEvent();
    }
}

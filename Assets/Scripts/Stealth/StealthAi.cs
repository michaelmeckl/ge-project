using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class StealthAi : MonoBehaviour
{
    public NavMeshAgent agent;
    public Transform player;
    private StealthView enemy;
    public LayerMask Ground, PLayer;
    public List<GameObject> waypoints;
    public bool useWaypoints = false;
    public float closeDetectionRange = 5f;
    public GameController Controller;
    public int damage = 20;
    public Vector3 walkPoint;
    public bool notPushed = true;
    public bool walkPointSet;
    public float walkPointRange;
    public float attackRange = 0;
    private bool alreadyAttacked = false;
    public float attackSpeed;
    public bool chasingPlayer = false;
    public bool playerFound;
    public GameObject oldPosition;
    private float originalAttackRange;
    private Animator _animator;
    private void Start()
    {
        if (gameObject.GetComponent<Animator>())
        {
            _animator = GetComponent<Animator>();
        }
        enemy = this.GetComponent<StealthView>();
        agent = GetComponent<NavMeshAgent>();
        Controller = GameObject.Find("GameManager").GetComponent<GameController>();
        var Player = GameObject.Find("Player(Clone)");
        player = Player.transform.GetChild(4).transform;
        originalAttackRange = attackRange;
        oldPosition = new GameObject();
    }

    /*
        Cleanup soon TM
    */
    private void FixedUpdate()
    {
        playerFound = enemy.foundPlayer;
        if (!enemy.foundPlayer && gameObject.GetComponent<StealthView>().IsNotCombatant)
        {
            if (transform.position == oldPosition.transform.position)
            {
                StartCoroutine(ResetWaypoints());
            }

            oldPosition.transform.position = transform.position;
        }
    }

    private void Update()
    {
        if (Controller.Paused)
        {
            agent.isStopped = true;
            if (_animator)
            {
                _animator.SetFloat(Animator.StringToHash("Speed"), 0f);
                _animator.SetFloat(Animator.StringToHash("MotionSpeed"), 0f);

            }

        }
        else
        {
            agent.isStopped = false;

            if (enemy.foundPlayer && gameObject.GetComponent<StealthView>().IsNotCombatant)
            {
                attackRange = 20;
            }
            else
            {
                attackRange = originalAttackRange;
            }

            CheckPlayerCombat();
            CheckCloseDetection();
            if (!chasingPlayer && !useWaypoints)Patroling();
            if (!chasingPlayer && useWaypoints) PatrolWaypoints();
            if (chasingPlayer && enemy.foundPlayer && Vector3.Distance(agent.transform.position, player.transform.position) > attackRange)
            {
                if (!gameObject.GetComponent<StealthView>().IsNotCombatant)
                {
                    chasePlayer();

                }
                
            }
            if(Vector3.Distance(agent.transform.position, player.transform.position) < attackRange)
            {
                if (_animator)
                {
                    _animator.SetFloat(Animator.StringToHash("Speed"), 0f);
                    _animator.SetFloat(Animator.StringToHash("MotionSpeed"), 0f);
                }
                 
                agent.SetDestination(agent.transform.position);
                if (!alreadyAttacked)
                {
                
                    attackPlayer();
                }
            
            }
        }
    }

    private void CheckCloseDetection()
    {
        if (Vector3.Distance(agent.transform.position, player.transform.position) < closeDetectionRange)
        {
            chasePlayer();
        }
    }

    private void CheckPlayerCombat()
    {
        if (enemy.foundPlayer)
        {
            chasingPlayer = true;
        }
        else
        {
            Vector3 distanceToWalkPoint = transform.position - walkPoint; 
            if (distanceToWalkPoint.magnitude < 1.5f)
            {
                chasingPlayer = false;
                walkPointSet = false;
            }
            // chases player if they are too close 


        }
        
        
    }

    private void Patroling()
    {
        agent.speed = 12f;
        if (_animator)
        {
            _animator.SetFloat(Animator.StringToHash("Speed"), 10f);
            _animator.SetFloat(Animator.StringToHash("MotionSpeed"), 1f);
        }
        if (!walkPointSet) SearchWalkPoint();
        if (walkPointSet)

            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        if (distanceToWalkPoint.magnitude < 3f)
            walkPointSet = false;
    }

    private void chasePlayer()
    {
        if (_animator)
        {
            _animator.SetFloat(Animator.StringToHash("Speed"), 10f);
            _animator.SetFloat(Animator.StringToHash("MotionSpeed"), 1f);
        }
        if(player.GetComponent<GameHandler>().healthSystem.GetHealth() != 0) agent.SetDestination(player.position);
        walkPoint = player.position;
        walkPointSet = true;
    }

    private void attackPlayer()
    {
        transform.LookAt(player);
        if (_animator)
        {
            _animator.SetFloat(Animator.StringToHash("Speed"), 0f);
            _animator.SetFloat(Animator.StringToHash("MotionSpeed"), 0f);
        }
        if (!alreadyAttacked && notPushed)
        {
            // do attack animation i
            if (_animator)
            {
                _animator.SetTrigger("AttackTrigger");
            }

            player.GetComponent<GameHandler>().healthSystem.Damage(damage);
            alreadyAttacked = true;
        }
        Invoke(nameof(ResetAttack), attackSpeed);
    }

    private void PatrolWaypoints()
    {
        agent.speed = 5f;
        if (_animator)
        {
            _animator.SetFloat(Animator.StringToHash("Speed"), 10f);
            _animator.SetFloat(Animator.StringToHash("MotionSpeed"), 1f);

        }
        if (!walkPointSet) GetWayPoint();
        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }

    private void GetWayPoint()
    {
        walkPoint = waypoints.First().transform.position;

        // Shift the array
        GameObject waypoint = waypoints.First();
        waypoints.Remove(waypoints.First());
        waypoints.Add(waypoint);
        walkPointSet = true;
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }
    private void SearchWalkPoint()
    {
        Vector3 randomDirection = Random.insideUnitSphere * walkPointRange;
        randomDirection += transform.position;
        if(randomDirection.x != float.PositiveInfinity)
        {
            NavMeshHit navHit;
            NavMesh.SamplePosition(randomDirection, out navHit, walkPointRange, -1);

            walkPoint = navHit.position;
            walkPointSet = true;
        }
    }

    IEnumerator ResetWaypoints()
    {
        Vector3 checkPosi = transform.position;
        yield return new WaitForSeconds(1);
        if(checkPosi == transform.position)
        {
            chasingPlayer = false;
            walkPointSet = false;
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StealthObservator : MonoBehaviour
{
    [SerializeField]
    private List<StealthView> _searchers;

    internal bool Discovered;

    private void Start()
    {
        Discovered = false;
    }

    private void Update()
    {
        if(!Discovered)
            Discovered = _searchers.Any(x=>x.foundPlayer);
    }

}

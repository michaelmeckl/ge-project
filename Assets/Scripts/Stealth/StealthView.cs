using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StealthView : MonoBehaviour
{
    public float viewRadius;
    [Range(0,360)]
    public float viewAngle;

    public GameObject player;
    public GameObject view;
    public LayerMask obstacleMask;

    public float meshResolution;
    public MeshFilter viewMeshFilter;
    public bool IsNotCombatant = false;
    Mesh viewMesh;

    public Material matRef;
    public Material matRef2;

    public bool foundPlayer = false;
    private void Start()
    {
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;

        player = GameObject.Find("Player(Clone)").transform.GetChild(4).gameObject;


        StartCoroutine("FindTargetsWithDelay", .05f);
    }

    IEnumerator FindTargetsWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
    }

    private void LateUpdate()
    {
        if (IsNotCombatant)
        {
            DrawFieldOFView();
        }
    }
    void FindVisibleTargets()
    {
        viewMeshFilter.GetComponent<Renderer>().material = matRef2;
        foundPlayer = false;
        if(Vector3.Distance(transform.position, player.transform.position) < viewRadius)
        {
            Vector3 dirToTarget = (player.transform.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                if (!Physics.Raycast(transform.position, dirToTarget, viewRadius, obstacleMask))
                {
                    viewMeshFilter.GetComponent<Renderer>().material = matRef;
                    if (player.GetComponent<GameHandler>().healthSystem.GetHealth() != 0)
                    {
                        foundPlayer = true;
                    }
                }

            }
            
        }
    }

    
    void DrawFieldOFView()
    {
        float groundY = 0;
        RaycastHit hit = new RaycastHit();
        if(Physics.Raycast(view.transform.position, Vector3.down, out hit )){
            groundY = hit.distance;
        }

        int rayCount = Mathf.RoundToInt(viewAngle * meshResolution);
        float stepAngleSize = viewAngle / rayCount;
        List<Vector3> viewPoints = new List<Vector3>();
        for(int i = 0; i <= rayCount; i++)
        {
            float angle = transform.eulerAngles.y - viewAngle / 2 + stepAngleSize * i;
            ViewCastInfo newViewCast = ViewCast(angle);
            viewPoints.Add(newViewCast.point);
        }

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for(int i = 0; i < vertexCount-1; i++)
        {
            vertices[i + 1] = new Vector3(transform.InverseTransformPoint(viewPoints[i]).x, - groundY,transform.InverseTransformPoint(viewPoints[i]).z);

            if(i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }

        }
        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }


    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
            
        }       
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit hit;

       if(Physics.Raycast(transform.position, dir, out hit, viewRadius, obstacleMask))
        {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }else
        {
            return new ViewCastInfo(false, transform.position + dir * viewRadius, viewAngle, globalAngle);
        }
    }

    public struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
        {
            hit = _hit;
            point = _point;
            dst = _dst;
            angle = _angle;
        }
    }


}

